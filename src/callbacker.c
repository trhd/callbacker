/**
 * callbacker -- A multithreaded callback queue / event loop library.
 * Copyright (C) 2018-2019 Hemmo Nieminen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "callbacker.h"

#include <assert.h>
#include <dsac/atomic_debug_flags.h>
#include <dsac/condition.h>
#include <dsac/lock.h>
#include <dsac/lockless_fifo_template.h>
#include <dsac/lockless_lifo_template.h>
#include <dsac/splay_tree.h>
#include <errno.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/epoll.h>
#include <sys/eventfd.h>
#include <sys/ioctl.h>
#include <sys/signalfd.h>
#include <sys/timerfd.h>
#include <time.h>
#include <unistd.h>

#ifndef TFD_IOC_SET_TICKS
// warning Horrible hack here!
#define TFD_IOC_SET_TICKS 1074287616ul
#endif

ATOMIC_DEBUG_FLAGS_ENUM(callbacker_data_atomic_debug_flag,
		CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_FREE,
		CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_RUNNING,
		_CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_COUNT
	)

ATOMIC_DEBUG_FLAGS_ENUM(callbacker_atomic_debug_flag,
		CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED,
		_CALLBACKER_ATOMIC_DEBUG_FLAG_COUNT
	)

#define CALLBACKER_EPOLL_MAX_EVENTS 3
#define CALLBACKER_EPOLL_TIMEOUT 10000
#define CALLBACKER_SIGNALFD_MAX_EVENTS 3

#define __Q(a) #a
#define _Q(a) __Q(a)

#if defined(CALLBACKER_DEBUG) || defined(UNIT_TESTING)
# define debug(...)         fprintf(stdout, "DEBUG:         callbacker+" _Q(__LINE__) ": " __VA_ARGS__)
# define debug_warning(...) fprintf(stderr, "DEBUG_WARNING: callbacker+" _Q(__LINE__) ": " __VA_ARGS__)
# define debug_error(...)   fprintf(stderr, "DEBUG_ERROR:   callbacker+" _Q(__LINE__) ": " __VA_ARGS__)
# define warning(...)       fprintf(stderr, "WARNING:       callbacker+" _Q(__LINE__) ": " __VA_ARGS__)
# define error(...)         fprintf(stderr, "ERROR:         callbacker+" _Q(__LINE__) ": " __VA_ARGS__)
#else
# define debug(...)         do {} while (false)
# define debug_error(...)   do {} while (false)
# define debug_warning(...) do {} while (false)
# define warning(...)       fprintf(stderr, "WARNING: callbacker: " __VA_ARGS__)
# define error(...)         fprintf(stderr, "ERROR:   callbacker: " __VA_ARGS__)
#endif

#define container_of(PTR, TYPE, MEMBER) ((TYPE *)(((unsigned long)PTR) - offsetof(TYPE, MEMBER)))

#define assert_timer_data_ptr(PTR) \
	assert(container_of(PTR, struct callbacker_data, timer)->type == CALLBACKER_DATA_TYPE_TIMER)

#define assert_io_data_ptr(PTR) \
	assert(container_of(PTR, struct callbacker_data, io)->type == CALLBACKER_DATA_TYPE_IO)

struct callbacker_data;

#if CALLBACKER_FAIR_SCHEDULING
LOCKLESS_FIFO_TEMPLATE(struct callbacker_data, callback_queue)
#else
LOCKLESS_LIFO_TEMPLATE(struct callbacker_data, callback_queue)
#endif

LOCKLESS_LIFO_TEMPLATE(struct callbacker_data, cache)

struct io_callback
{
	void * CALLBACKER_NULLABLE user_data;
	enum callbacker_io_type (*CALLBACKER_NONNULL function)(int fd,
	                                                       enum callbacker_io_type type,
	                                                       void * CALLBACKER_NULLABLE user_data);
};

struct io_data
{
	atomic_int file_descriptor;
	atomic_bool triggered;

	atomic_uint_least8_t subscribed_types;
	atomic_uint_least8_t detected_types;

	struct io_callback callback;

	uint8_t priority;
	callback_queue_t handle;
};

struct timer_callback
{
	void * user_data;
	enum callbacker_timer_type (*function)(callbacker_timer_id_t CALLBACKER_NONNULL,
	                                       enum callbacker_timer_type,
	                                       void * CALLBACKER_NULLABLE user_data);
};

struct timer_data
{
	int file_descriptor;

	atomic_bool triggered;
	atomic_bool expired;
	struct timer_callback callback;

	uint8_t priority;
	callback_queue_t handle;
};

struct signal_callback
{
	void (*CALLBACKER_NONNULL function)(int signal, void * CALLBACKER_NULLABLE user_data);
	void * CALLBACKER_NULLABLE user_data;
};

struct signal_data
{
	int file_descriptor;

	int received_signal;
	struct signal_callback callback;

	uint8_t priority;
	callback_queue_t handle;
};

struct direct_callback
{
	void * user_data;
	void (*function)(void *);
};

struct direct_data
{
	struct direct_callback callback;
	callback_queue_t handle;
};

struct callbacker_data
{
	union
	{
		cache_t cache;
		struct io_data io;
		struct timer_data timer;
		struct signal_data signal;
		struct direct_data direct;
	};
	enum data_type
	{
		CALLBACKER_DATA_TYPE_DIRECT,
		CALLBACKER_DATA_TYPE_IO,
		CALLBACKER_DATA_TYPE_SIGNAL,
		CALLBACKER_DATA_TYPE_TIMER
	} type : 8;
	ATOMIC_DEBUG_FLAGS(_CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_COUNT)
};

struct callbacker
{
	int epoll_fd;

	int queue_event_fd;
	int kill_event_fd;
	callback_queue_t queues[CALLBACKER_QUEUE_COUNT];
	atomic_uint volatile queue_usage;

	cache_t cache;

	atomic_uint thread_count;
	atomic_uint threads_running;
	atomic_uint threads_trying;

	sigset_t sigmask;

	struct lock thread_lock;
	struct condition thread_condition;

	ATOMIC_DEBUG_FLAGS(_CALLBACKER_ATOMIC_DEBUG_FLAG_COUNT)
};

enum callbacker_context_atomic_flag
{
	CALLBACKER_CONTEXT_ATOMIC_FLAG_EXEC,
	CALLBACKER_CONTEXT_ATOMIC_FLAG_DEDICATED,
	_CALLBACKER_CONTEXT_ATOMIC_FLAG_COUNT
};

static _Thread_local
struct callbacker_context
{
	atomic_bool volatile running;
	ATOMIC_FLAGS(_CALLBACKER_CONTEXT_ATOMIC_FLAG_COUNT);
} CONTEXT = {
	.running = false
};

static struct callbacker callbacker;

#if !defined(NDEBUG) || defined(UNIT_TESTING)

static void
assert_sane_io_type(enum callbacker_io_type t)
{
	switch ((int)t)
	{
		case CALLBACKER_IO_TYPE_NONE:
		case CALLBACKER_IO_TYPE_ANY:
		case CALLBACKER_IO_TYPE_WRITE:
		case CALLBACKER_IO_TYPE_READ:
		case CALLBACKER_IO_TYPE_READWRITE:
		case CALLBACKER_IO_TYPE_KEEP:
			break;
		default:
			abort();
	}
}

#else
# define assert_sane_io_type(...)
#endif

static void
mask_registered_signals()
{
	static _Thread_local bool done = false;

	if (!done && sigprocmask(SIG_BLOCK, &callbacker.sigmask, NULL))
	{
		debug("Failed to mask registered signals.\n");
		abort();
	}
	else
		done = true;
}

static void
context_initialize()
{
	if (!CONTEXT.running)
	{
		atomic_init(&CONTEXT.running, true);
		atomic_flags_initialize(&CONTEXT);
		mask_registered_signals();
	}
}

static void
context_uninitialize()
{
	debug_warning("Not unmasking possibly masked signals.\n");
}

size_t
callbacker_data_size()
{
	return sizeof(struct callbacker_data);
}

static void
free_data(struct callbacker_data * CALLBACKER_NONNULL d)
{
	assert(d);

	cache_t * e = &d->cache;

	cache_initialize(e);
	cache_set(e, d);

	atomic_debug_flags_assert_not(d, CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_FREE);
	atomic_debug_flags_set(d, CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_FREE);

	cache_push(&callbacker.cache, e);
}

static void
initialize_cache(void * c, size_t cl)
{
	assert(c);
	assert(cl);

	unsigned long d = cl / sizeof(struct callbacker_data);

	debug("Initializing %zd byte cache for %lu holders of size %zd.\n",
			cl, d, sizeof(struct callbacker_data));

	cache_initialize(&callbacker.cache);

	for (struct callbacker_data * u = (struct callbacker_data *)c ; d ; u++, d--)
	{
		atomic_debug_flags_initialize(u);
		free_data(u);
	}
}

/**
 * Use type specific allocators to avoid multiple layers of NULL pointer
 * checking during usage.
 */
#define _type_allocator(TYPE) allocate_##TYPE##_data
#define _type_data(TYPE) TYPE##_data
#define gen_data_allocator(TYPE_NAME, TYPE) \
	static struct TYPE_NAME##_data * CALLBACKER_NULLABLE allocate_##TYPE_NAME##_data() \
	{ \
		cache_t * r = cache_pop(&callbacker.cache); \
		struct callbacker_data * d; \
\
		if (!r) \
		{ \
			error("Failed to allocate a new " _Q(TYPE_NAME) " data structure.\n"); \
			errno = ENOMEM; \
			return NULL; \
		} \
\
		atomic_debug_flags_assert(container_of(r, struct callbacker_data, cache), \
		                          CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_FREE); \
\
		atomic_debug_flags_clear(container_of(r, struct callbacker_data, cache), \
		                         CALLBACKER_DATA_ATOMIC_DEBUG_FLAG_FREE); \
		d = cache_get(r); \
		assert(d); \
		d->type = TYPE; \
		callback_queue_set(&d->TYPE_NAME.handle, d); \
\
		return &cache_get(r)->TYPE_NAME; \
	} \
\
	static void CALLBACKER_UNUSED free_##TYPE_NAME##_data(struct TYPE_NAME##_data * CALLBACKER_NONNULL data) \
	{ \
		free_data(container_of(data, struct callbacker_data, TYPE_NAME)); \
	}

gen_data_allocator(signal, CALLBACKER_DATA_TYPE_SIGNAL)
gen_data_allocator(timer, CALLBACKER_DATA_TYPE_TIMER)
gen_data_allocator(io, CALLBACKER_DATA_TYPE_IO)
gen_data_allocator(direct, CALLBACKER_DATA_TYPE_DIRECT)

#undef _type_allocator
#undef _type_data
#undef gen_data_allocator

#define wait_a_sec(...) _wait_a_sec(__VA_ARGS__, __LINE__)

static void
_wait_a_sec(struct timespec * CALLBACKER_NONNULL delay, unsigned max_sec, unsigned CALLBACKER_UNUSED line)
{
	debug_warning("You are sleeping (from line %u). This should be avoided.\n", line);

	nanosleep(delay, NULL);

	if (!delay->tv_nsec && !delay->tv_sec)
		delay->tv_nsec = 1000;
	else if (delay->tv_nsec < 1000*1000*1000 && !delay->tv_sec)
		delay->tv_nsec *= 2;
	else if (delay->tv_nsec)
	{
		delay->tv_nsec = 0;
		delay->tv_sec = 1;
	}
	else if (delay->tv_sec < max_sec)
		delay->tv_sec += 1;
}

static bool
try_schedule_epoll(int fd, struct epoll_event * e)
{
	assert(fd > 0);
	assert(e);

	if (epoll_ctl(callbacker.epoll_fd, EPOLL_CTL_ADD, fd, e) == -1)
	{
		assert(errno == EEXIST);
		debug("Failed to schedule epoll for file descriptor %d: %s.\n",
				fd, strerror(errno));
		return true;
	}
	return false;
}

static void
schedule_epoll(int fd, struct epoll_event * e)
{
	assert(fd > 0);
	assert(e);

	if (epoll_ctl(callbacker.epoll_fd, EPOLL_CTL_ADD, fd, e) == -1)
	{
		debug("Failed to schedule epoll for file descriptor %d: %s.\n",
				fd, strerror(errno));
		abort();
	}
}

static bool
try_reschedule_epoll(int fd, struct epoll_event * e)
{
	assert(fd > 0);
	assert(e);

	if (epoll_ctl(callbacker.epoll_fd, EPOLL_CTL_MOD, fd, e) == -1)
	{
		debug("Failed to REschedule epoll for file descriptor %d: "
		      "%s.\n",
		      fd,
		      strerror(errno));
		if (errno == ENOENT)
			return true;
		abort();
	}
	return false;
}

static void
reschedule_epoll(int fd, struct epoll_event * e)
{
	assert(fd > 0);
	assert(e);

	if (epoll_ctl(callbacker.epoll_fd, EPOLL_CTL_MOD, fd, e) == -1)
	{
		error("Failed to REschedule epoll for file descriptor %d: %s.\n",
				fd, strerror(errno));
		abort();
	}
}

static void
try_unschedule_epoll(int fd)
{
	if (epoll_ctl(callbacker.epoll_fd, EPOLL_CTL_DEL, fd, NULL) == -1)
	{
		if (errno == EBADF || errno == ENOENT)
			debug("Failed to remove from epoll. File descriptor closed outside of callbacker perhaps?\n");
		else
		{
			error("Failed to (try) unschedule epoll: %s.\n", strerror(errno));
			abort();
		}
	}
}

static void
unschedule_epoll(int fd)
{
	if (epoll_ctl(callbacker.epoll_fd, EPOLL_CTL_DEL, fd, NULL) == -1)
	{
		error("Failed to unschedule epoll: %s.\n", strerror(errno));
		abort();
	}
}

static void
io_sub_to_epoll(struct io_data * io, struct epoll_event * e)
{
	assert(io);
	assert(e);

	enum callbacker_io_type types = atomic_load(&io->subscribed_types);

	e->data.ptr = container_of(io, struct callbacker_data, io);
	e->events = ((types & CALLBACKER_IO_TYPE_READ) ? (EPOLLIN | EPOLLRDHUP) : 0)
	           | ((types & CALLBACKER_IO_TYPE_WRITE) ? EPOLLOUT : 0)
	           | EPOLLONESHOT;
}

static void
signal_sub_to_epoll(struct signal_data const * s, struct epoll_event * e)
{
	assert(s);
	assert(e);

	e->data.ptr = container_of(s, struct callbacker_data, signal);
	e->events = EPOLLIN | EPOLLONESHOT;
}

static void
schedule_io(struct io_data * io)
{
	assert(io);

	struct epoll_event e;
	struct timespec d = {0};

	io_sub_to_epoll(io, &e);

	while (try_schedule_epoll(io->file_descriptor, &e))
		wait_a_sec(&d, 30);
}

static void
schedule_timer(struct timer_data const * t)
{
	assert(t);

	struct epoll_event e;

	e.data.ptr = container_of(t, struct callbacker_data, timer);
	e.events = EPOLLIN | EPOLLRDHUP | EPOLLONESHOT;

	schedule_epoll(t->file_descriptor, &e);
}

static void
try_unschedule_io(struct io_data * CALLBACKER_NONNULL io)
{
	assert(io);

	int fd = io->file_descriptor;

	if (fd && atomic_compare_exchange_strong(&io->file_descriptor, &fd, 0))
		try_unschedule_epoll(fd);
}

static bool
try_reschedule_io(struct io_data * io)
{
	assert(io);

	struct epoll_event e;

	io_sub_to_epoll(io, &e);

	return try_reschedule_epoll(io->file_descriptor, &e);
}

static void
reschedule_signal(struct signal_data const * s)
{
	assert(s);

	struct epoll_event e;

	signal_sub_to_epoll(s, &e);

	reschedule_epoll(s->file_descriptor, &e);
}

static void
reschedule_timer(struct timer_data const * t)
{
	assert(t);

	struct epoll_event e;

	e.data.ptr = container_of(t, struct callbacker_data, timer);
	e.events = EPOLLIN | EPOLLRDHUP | EPOLLONESHOT;

	reschedule_epoll(t->file_descriptor, &e);
}

static void
schedule_event_queue_epoll()
{
	struct epoll_event e = {0};

	e.events = EPOLLIN;
	e.data.ptr = &callbacker.queue_event_fd;

	schedule_epoll(callbacker.queue_event_fd, &e);
}

static void
schedule_kill_event_epoll()
{
	struct epoll_event e = {0};

	e.events = EPOLLIN;
	e.data.ptr = &callbacker.kill_event_fd;

	schedule_epoll(callbacker.kill_event_fd, &e);
}

static bool
schedule_queue_events()
{
	assert(!callbacker.queue_event_fd);

	callbacker.queue_event_fd = eventfd(0, EFD_SEMAPHORE | EFD_NONBLOCK);

	if (callbacker.queue_event_fd < 0)
		return true;

	schedule_event_queue_epoll();
	return false;
}

static bool
schedule_kill_events()
{
	assert(!callbacker.kill_event_fd);

	callbacker.kill_event_fd = eventfd(0, EFD_SEMAPHORE | EFD_NONBLOCK);

	if (callbacker.kill_event_fd < 0)
		return true;

	schedule_kill_event_epoll();
	return false;
}

static void
queues_initialize()
{
	for (int i = 0 ; i < CALLBACKER_QUEUE_COUNT ; i++)
		callback_queue_initialize(&callbacker.queues[i]);

	atomic_init(&callbacker.queue_usage, 0);
}

static void
unschedule_event_queue_epoll()
{
	unschedule_epoll(callbacker.queue_event_fd);
}

static bool
events_initialize()
{
	if (schedule_queue_events())
		return true;

	if (schedule_kill_events())
	{
		unschedule_event_queue_epoll();
		return true;
	}

	return false;
}

static void
unschedule_kill_event_epoll()
{
	unschedule_epoll(callbacker.kill_event_fd);
}

static void
events_uninitialize()
{
	unschedule_event_queue_epoll();
	unschedule_kill_event_epoll();

	if (close(callbacker.queue_event_fd))
		abort();

	if (close(callbacker.kill_event_fd))
		abort();

	callbacker.queue_event_fd = 0;
	callbacker.kill_event_fd = 0;
}

static bool
epoll_initialize()
{
	callbacker.epoll_fd = epoll_create1(0);

	if (callbacker.epoll_fd < 0)
	{
		assert(errno != EINVAL);
		return true;
	}

	return false;
}

static void
epoll_uninitialize()
{
	if (close(callbacker.epoll_fd))
	{
		debug("Failed to uninitialize epoll file descriptor.\n");
		abort();
	}
}

bool
callbacker_initialize(void * b, size_t l)
{
	assert(b);
	assert(l);
	atomic_debug_flags_assert_not(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	atomic_init(&callbacker.thread_count, 0);
	atomic_init(&callbacker.threads_running, 0);
	atomic_init(&callbacker.threads_trying, 0);

	initialize_cache(b, l);
	sigemptyset(&callbacker.sigmask);
	queues_initialize();

	if (!epoll_initialize())
	{
		if (!events_initialize())
		{
			if (!lock_initialize(&callbacker.thread_lock))
			{
				if (!condition_initialize(&callbacker.thread_condition, &callbacker.thread_lock))
				{
					atomic_debug_flags_initialize(&callbacker);
					atomic_debug_flags_set(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);
					return false;
				}
				lock_uninitialize(&callbacker.thread_lock);
			}
			events_uninitialize();
		}
		epoll_uninitialize();
	}

	return true;
}

static void
generate_queue_event()
{
	static uint64_t const one = 1;

	atomic_fetch_add(&callbacker.queue_usage, 1);
	if (write(callbacker.queue_event_fd, &one, sizeof(one)) <= 0)
	{
		debug("Failed to write to the callback event queue file descriptor.\n");
		abort();
	}
}

void
callbacker_uninitialize()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	if (callbacker_threads_count_set(0))
		abort();

	lock_acquire(&callbacker.thread_lock);

	while (atomic_load(&callbacker.threads_running) > 0)
		condition_wait(&callbacker.thread_condition);

	while (atomic_load(&callbacker.threads_trying) > 0)
		condition_wait(&callbacker.thread_condition);

	events_uninitialize();
	epoll_uninitialize();

	atomic_debug_flags_clear(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	lock_release(&callbacker.thread_lock);

	condition_uninitialize(&callbacker.thread_condition);
	lock_uninitialize(&callbacker.thread_lock);

#ifdef UNIT_TESTING
	/**
	 * Introduce a delay before returning from the
	 * callbacker_uninitialize() to ensure all other threads have had time
	 * to shutdown and valgrind will not complain about them.
	 */
	if (getenv("VALGRIND") && atoi(getenv("VALGRIND")) > 0)
	{
		debug("Sleeping to give threads time to die...\n");
		usleep(500000);
	}
#endif
}

static void
generate_kill_events(uint64_t count)
{
	debug("Generating %lu kill events.\n", count);

	if (write(callbacker.kill_event_fd, &count, sizeof(count)) <= 0)
	{
		debug("Failed to write to the kill event file descriptor.\n");
		abort();
	}
}

unsigned int
callbacker_threads_count_get()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	return atomic_load(&callbacker.thread_count);
}

static uint8_t
shift_priority(int8_t pr)
{
	static_assert(CALLBACKER_QUEUE_COUNT % 2,
			"The number of priority levels / callbackerer queue_callbackes must be odd.");
	static_assert(CALLBACKER_HIGH_PRIORITY <= 0,
			"Expecting high priority to be negative (or at least zero).");
	static_assert(CALLBACKER_HIGH_PRIORITY < CALLBACKER_LOW_PRIORITY,
			"Expecting high priority (index) to preceed low priority.");
	static_assert(CALLBACKER_HIGH_PRIORITY <= INT8_MAX,
			"Expecting priority to be at most INT8_MAX.");
	static_assert(CALLBACKER_LOW_PRIORITY >= INT8_MIN,
			"Expecting priority to be at least INT8_MIN.");

	uint8_t p = pr - CALLBACKER_HIGH_PRIORITY;
	assert(p <= 2*CALLBACKER_LOW_PRIORITY);

	return p;
}

static void
queue_callback(callback_queue_t * q, uint8_t pr)
{
	assert(q);
	callback_queue_push(&callbacker.queues[pr], q);
	generate_queue_event();
}

static void
consume_queue_event()
{
	uint64_t num;

	if (read(callbacker.queue_event_fd, &num, sizeof(num)) <= 0)
		debug("Failed to read from callback event queue file descriptor.\n");
	else
		assert(num == 1);
}

static bool
consume_kill_event()
{
	uint64_t num = 0;

	if (read(callbacker.kill_event_fd, &num, sizeof(num)) <= 0 && errno != EAGAIN)
	{
		printf("error: %s\n", strerror(errno));
		abort();
	}

	assert(num == 0 || num == 1);

	if (num)
	{
		debug("Read a single kill event.\n");
		atomic_store(&CONTEXT.running, false);
		return false;
	}

	return true;
}

bool
callbacker_run(int8_t pr, void (*fn) (void *), void * a)
{
	assert(pr >= CALLBACKER_HIGH_PRIORITY);
	assert(pr <= CALLBACKER_LOW_PRIORITY);
	assert(fn);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	struct direct_data * d = allocate_direct_data();

	if (!d)
	{
		debug("Failed to allocate memory for a new task.\n");
		return true;
	}

	d->callback.function = fn;
	d->callback.user_data = a;

	queue_callback(&d->handle, shift_priority(pr));

	return false;
}

unsigned int
callbacker_get_thread_size()
{
	return sizeof(struct callbacker_context);
}

static enum callbacker_io_type
invoke_io_callback(struct io_data * io)
{
	assert(io);

	return io->callback.function(io->file_descriptor,
	                             io->detected_types
	                                             | (atomic_exchange(&io->triggered, false)
	                                                                ? CALLBACKER_IO_TYPE_TRIGGER
	                                                                : CALLBACKER_IO_TYPE_NONE),
	                             io->callback.user_data);
}

static bool
invoke_timer_callback(struct timer_data * t)
{
	assert(t);

	enum callbacker_timer_type e
	                = atomic_exchange(&t->triggered, false)
	                                  ? CALLBACKER_TIMER_TYPE_TRIGGER
	                                  : CALLBACKER_TIMER_TYPE_NONE
	                | atomic_exchange(&t->expired, false)
	                                  ? CALLBACKER_TIMER_TYPE_EXPIRATION
	                                  : CALLBACKER_TIMER_TYPE_NONE;

	return t->callback.function(t, e, t->callback.user_data);
}

static enum callbacker_io_type
epoll_events_to_io_type(uint32_t epoll_events)
{
	return ((epoll_events & EPOLLIN) ? CALLBACKER_IO_TYPE_READ : 0)
	       | ((epoll_events & EPOLLOUT) ? CALLBACKER_IO_TYPE_WRITE : 0)
	       | ((epoll_events & (EPOLLRDHUP | EPOLLHUP)) ? CALLBACKER_IO_TYPE_HANGUP | CALLBACKER_IO_TYPE_ERROR : 0)
	       | ((epoll_events & EPOLLERR) ? CALLBACKER_IO_TYPE_ERROR : 0);
}

static void
try_trigger_io(struct io_data * CALLBACKER_NONNULL io, enum callbacker_io_type t)
{
	assert(io);
	assert(t & (CALLBACKER_IO_TYPE_TRIGGER | CALLBACKER_IO_TYPE_ANY));
	assert(!(t & (~(CALLBACKER_IO_TYPE_TRIGGER | CALLBACKER_IO_TYPE_ANY))));

	uint8_t e = 0;

	if (atomic_compare_exchange_strong(&io->detected_types, &e, t))
		callback_queue_push(&callbacker.queues[io->priority], &io->handle);
}

static void
trigger_signal(struct callbacker_data * CALLBACKER_NONNULL d)
{
	assert(d);

	callback_queue_push(&callbacker.queues[d->signal.priority],
	                    &d->signal.handle);
}

static void
trigger_timer(struct timer_data * CALLBACKER_NONNULL t)
{
	assert(t);

	atomic_store(&t->expired, true);
	callback_queue_push(&callbacker.queues[t->priority], &t->handle);
}

static void
try_trigger_subscription(struct callbacker_data * d, uint32_t epevs)
{
	switch (d->type)
	{
		case CALLBACKER_DATA_TYPE_IO:
			try_trigger_io(&d->io, epoll_events_to_io_type(epevs));
			break;
		case CALLBACKER_DATA_TYPE_TIMER:
			trigger_timer(&d->timer);
			break;
		case CALLBACKER_DATA_TYPE_SIGNAL:
			trigger_signal(d);
			break;
		default:
			error("Received a callbacker data structure of unexpected type: %u.\n", d->type);
			abort();
			break;
	}

	generate_queue_event();
}

static void
io_die(struct io_data * CALLBACKER_NONNULL io)
{
	assert(io);

	try_unschedule_io(io);
	free_io_data(io);
}

static void
handle_io_callback(struct io_data * CALLBACKER_NONNULL io)
{
	assert(io);

	enum callbacker_io_type r = invoke_io_callback(io);
	assert(!(r & (~(CALLBACKER_IO_TYPE_ANY | CALLBACKER_IO_TYPE_KEEP))));
	assert(!((r & CALLBACKER_IO_TYPE_ANY) && (r & CALLBACKER_IO_TYPE_KEEP)));

	if (r & CALLBACKER_IO_TYPE_ANY)
		atomic_store(&io->subscribed_types, r);
	else if (!(r & CALLBACKER_IO_TYPE_KEEP))
	{
		io_die(io);
		return;
	}

	atomic_store(&io->detected_types, 0);

	if (io->triggered)
		try_trigger_io(io, CALLBACKER_IO_TYPE_ERROR);
	else
		try_reschedule_io(io);
}

static bool
timer_expired(int tfd)
{
	assert(tfd > 0);

	uint64_t tmp;

	if (read(tfd, &tmp, sizeof(tmp)) <= 0)
	{
		error("Failed to read from timerfd: %s.\n", strerror(errno));
		abort();
	}

	return tmp > 0;
}

static void
cancel_timer(struct timer_data * CALLBACKER_NONNULL t)
{
	assert(t);

	unschedule_epoll(t->file_descriptor);

	if (close(t->file_descriptor))
		abort();

	free_timer_data(t);
}

static void
handle_timer_callback(struct timer_data * CALLBACKER_NONNULL t)
{
	assert(t);

	if (timer_expired(t->file_descriptor)
			&& ! invoke_timer_callback(t))
		cancel_timer(t);
	else
		reschedule_timer(t);
}

static void
invoke_direct_callback(struct direct_callback const * cb)
{
	assert(cb);

	cb->function(cb->user_data);
}

static void
handle_signal_callback(struct signal_data * s)
{
	assert(s);

	struct signalfd_siginfo nfo;

	for (int i = CALLBACKER_SIGNALFD_MAX_EVENTS;
	     i && read(s->file_descriptor, &nfo, sizeof(nfo)) == sizeof(nfo);
	     i--)
		s->callback.function(nfo.ssi_signo, s->callback.user_data);

	reschedule_signal(s);
}

static void
handle_epoll_error()
{
	if (errno != EINTR)
	{
		error("epoll failed: %s\n", strerror(errno));
		abort();
	}
	debug("epoll_wait interrupted by a signal.\n");
}

static unsigned int
check_epoll_fd(struct epoll_event * CALLBACKER_NONNULL ev, unsigned int len)
{
	assert(ev);

	int t = epoll_wait(callbacker.epoll_fd, ev, len, 0);

	if (t < 0)
	{
		handle_epoll_error();
		return 0;
	}

	return t;
}

static bool
wait_epoll_fd(struct epoll_event * CALLBACKER_NONNULL ev)
{
	assert(ev);

	int t = epoll_wait(callbacker.epoll_fd, ev, 1, 10);

	if (t < 0)
	{
		handle_epoll_error();
		return true;
	}

	return t == 0;
}

static bool
check_kill_event()
{
	if (atomic_flags_get(&CONTEXT, CALLBACKER_CONTEXT_ATOMIC_FLAG_DEDICATED)
	    || (atomic_flags_get(&CONTEXT, CALLBACKER_CONTEXT_ATOMIC_FLAG_EXEC) && !callbacker.threads_running))
		return consume_kill_event();
	else
		debug("Nope. Not gonna kill miself (%lu).\n", pthread_self());
	return true;
}

static bool
process_epoll_events(struct epoll_event * CALLBACKER_NONNULL ev, unsigned int c)
{
	assert(ev);

	unsigned int p = c;

	while (c --> 0)
	{
		if (ev[c].data.ptr == &callbacker.queue_event_fd)
			consume_queue_event();
		else if (ev[c].data.ptr == &callbacker.kill_event_fd)
		{
			if (!check_kill_event())
				c = p = 0;
			else
				p--;
		}
		else
			try_trigger_subscription((struct callbacker_data *)ev[c].data.ptr, ev[c].events);
	}

	return p == 0;
}

static void
check_events()
{
	enum { ev_array_len = 10 };
	struct epoll_event ev[ev_array_len];

	for (unsigned int r = check_epoll_fd(ev, ev_array_len);
	     r;
	     r = check_epoll_fd(ev, ev_array_len))
	{
		if (process_epoll_events(ev, r))
			break;
	}
}

static void
wait_events()
{
	enum { ev_array_len = 10 };
	struct epoll_event ev[ev_array_len];

	for (unsigned int r = wait_epoll_fd(ev) ? 0 : 1;
	     r;
	     r = check_epoll_fd(ev, ev_array_len))
	{
		if (process_epoll_events(ev, r))
			break;
	}
}

static struct callbacker_data *
_pop()
{
	callback_queue_t * r;

	for (int i = 0 ; i < CALLBACKER_QUEUE_COUNT ; ++i)
	{
		r = callback_queue_pop(&callbacker.queues[i]);

		if (r)
		{
			atomic_fetch_sub(&callbacker.queue_usage, 1);
			return callback_queue_get(r);
		}
	}

	return NULL;
}

static struct callbacker_data *
pop_task_try()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	if (!CONTEXT.running)
		return NULL;

	check_events();

	if (CONTEXT.running)
		return _pop();

	return NULL;
}

static struct callbacker_data *
pop_task()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	if (!CONTEXT.running)
		return NULL;

	wait_events();

	if (CONTEXT.running)
		return _pop();

	return NULL;
}

static void
handle_callback(struct callbacker_data * CALLBACKER_NONNULL d)
{
	assert(d);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	switch (d->type)
	{
		case CALLBACKER_DATA_TYPE_DIRECT:
			invoke_direct_callback(&d->direct.callback);
			free_direct_data(&d->direct);
			break;
		case CALLBACKER_DATA_TYPE_IO:
			handle_io_callback(&d->io);
			break;
		case CALLBACKER_DATA_TYPE_SIGNAL:
			handle_signal_callback(&d->signal);
			break;
		case CALLBACKER_DATA_TYPE_TIMER:
			handle_timer_callback(&d->timer);
			break;
		default:
			error("Invalid callbacker data type: %u.\n", d->type);
			abort();
	}
}

static void
work()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	struct callbacker_data * w = pop_task();

	if (w)
	{
		handle_callback(w);

		while ((w = pop_task_try()))
			handle_callback(w);
	}
}

static bool
work_try()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	struct callbacker_data * w = pop_task_try();

	if (w)
	{
		handle_callback(w);
		return false;
	}

	return true;
}

static void *
dedicated_thread_main(void * p)
{
	assert(p);

	atomic_bool * done = (atomic_bool *)p;

	context_initialize();
	atomic_flags_set(&CONTEXT, CALLBACKER_CONTEXT_ATOMIC_FLAG_DEDICATED);

	atomic_fetch_add(&callbacker.thread_count, 1);
	atomic_fetch_add(&callbacker.threads_running, 1);

	lock_acquire(&callbacker.thread_lock);
	atomic_store(done, true);
	condition_broadcast(&callbacker.thread_condition);
	lock_release(&callbacker.thread_lock);

	debug("Entered thread main (dedicated) loop.\n");

	while (atomic_load(&CONTEXT.running))
		work();

	debug("Worker main (dedicated) loop ended.\n");

	lock_acquire(&callbacker.thread_lock);
	atomic_fetch_sub(&callbacker.threads_running, 1);
	condition_broadcast(&callbacker.thread_condition);
	lock_release(&callbacker.thread_lock);

	context_uninitialize();

	return NULL;
}

bool
thread_start()
{
	int e;
	pthread_t t;
	pthread_attr_t attr;
	atomic_bool done;

	debug("Creating a new thread thread.\n");

	atomic_init(&done, false);

	if (pthread_attr_init(&attr))
		return true;

	if ((e = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED)))
	{
		pthread_attr_destroy(&attr);
		errno = e;
		return true;
	}

	if ((e = pthread_create(&t, &attr, dedicated_thread_main, &done)))
	{
		pthread_attr_destroy(&attr);
		errno = e;
		return true;
	}

	debug("Started a thread %lu.\n", t);

	pthread_attr_destroy(&attr);

	lock_acquire(&callbacker.thread_lock);
	while (!atomic_load(&done))
		condition_wait(&callbacker.thread_condition);
	lock_release(&callbacker.thread_lock);

	return false;
}

bool
callbacker_threads_start(unsigned int c)
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);
	assert(c);

	debug("Adding %u threads.\n", c);

	for ( ; c ; c--)
	{
		if (thread_start())
			break;
	}

	return c;
}

void
callbacker_threads_stop(unsigned int d)
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	unsigned int c, n;

	do
	{
		c = callbacker.thread_count;
		n = (d && c >= d) ? c - d : 0;
	}
	while (!atomic_compare_exchange_weak(&callbacker.thread_count, &c, n));

	debug("Killing %u threads (%u still left).\n", d, callbacker.thread_count);
	generate_kill_events(c);
}

bool
callbacker_threads_count_set(unsigned int n)
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	int delta = n - atomic_load(&callbacker.thread_count);
	bool rv;

	if (delta > 0)
		rv = callbacker_threads_start(delta);
	else
	{
		rv = false;
		if (delta < 0)
			callbacker_threads_stop(-delta);
	}

	return rv;
}

void
callbacker_exec()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	context_initialize();
	atomic_flags_set(&CONTEXT, CALLBACKER_CONTEXT_ATOMIC_FLAG_EXEC);

	atomic_fetch_add(&callbacker.thread_count, 1);

	debug("Entering thread main (main) loop.\n");

	while (atomic_load(&CONTEXT.running))
		work();

	debug("Worker main (main) loop ended.\n");
	context_uninitialize();
}

bool
callbacker_exec_try()
{
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	bool rv = true;

	context_initialize();

	atomic_fetch_add(&callbacker.threads_trying, 1);

	debug("Entering thread main (try) loop.\n");

	if (!work_try())
	{
		rv = false;
		while (!work_try());
	}

	debug("Worker main (try) loop ended.\n");

	lock_acquire(&callbacker.thread_lock);
	atomic_fetch_sub(&callbacker.threads_trying, 1);
	condition_broadcast(&callbacker.thread_condition);
	lock_release(&callbacker.thread_lock);

	context_uninitialize();

	return rv;
}

callbacker_io_id_t
callbacker_io_start(int fd,
                    enum callbacker_io_type type,
                    int8_t pr,
                    enum callbacker_io_type (*cb)(int, enum callbacker_io_type, void *),
                    void * a)
{
	assert(fd > 0);
	assert_sane_io_type(type);
	assert(!(type & CALLBACKER_IO_TYPE_KEEP));
	assert(type);
	assert(pr <= CALLBACKER_LOW_PRIORITY);
	assert(pr >= CALLBACKER_HIGH_PRIORITY);
	assert(cb);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	struct io_data * io = allocate_io_data();

	if (!io)
		return NULL;

	io->priority = shift_priority(pr);
	io->callback.user_data = a;
	io->callback.function = cb;
	io->subscribed_types = type;
	io->triggered = false;
	io->detected_types = 0;
	io->file_descriptor = fd;

	schedule_io(io);

	return io;
}

static void
arm_signal_subscription(struct signal_data * s, sigset_t const * m)
{
	assert(s);

	struct epoll_event e;
	s->file_descriptor = signalfd(-1, m, SFD_NONBLOCK);

	if (s->file_descriptor < 0)
	{
		error("Failed to arm signal subsription.\n");
		abort();
	}

	signal_sub_to_epoll(s, &e);
	schedule_epoll(s->file_descriptor, &e);

	debug("Armed a signal subscription.\n");
}

void
_callbacker_signal_start(int8_t p, void (*cb) (int, void*), void * u, ...)
{
	assert(p <= CALLBACKER_LOW_PRIORITY);
	assert(p >= CALLBACKER_HIGH_PRIORITY);
	assert(cb);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);
	assert(!atomic_load(&callbacker.threads_running));

	int s;
	va_list ap;
	sigset_t m;
	struct signal_data * sd = allocate_signal_data();

	if (!sd)
		abort();

	sigemptyset(&m);

	va_start(ap, u);
	while ((s = va_arg(ap, int)) != -1)
	{
		assert(s > 0);
		assert(s <= SIGRTMAX);
		sigaddset(&m, s);
		sigaddset(&callbacker.sigmask, s);
		debug("Subscribing signal %d.\n", s);
	}
	va_end(ap);

	sd->callback.user_data = u;
	sd->callback.function = cb;
	sd->priority = shift_priority(p);

	arm_signal_subscription(sd, &m);
}

callbacker_timer_id_t
callbacker_timer_start(int8_t pr,
                       struct timespec const * d,
                       struct timespec const * i,
                       enum callbacker_timer_type (*cb)(callbacker_timer_id_t,
                                                        enum callbacker_timer_type,
                                                        void *),
                       void * a)
{
	assert(pr <= CALLBACKER_LOW_PRIORITY);
	assert(pr >= CALLBACKER_HIGH_PRIORITY);
	assert(cb);
	assert(d);
	assert(d->tv_sec || d->tv_nsec);
	assert(i);
	assert(i->tv_sec || i->tv_nsec);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	struct timer_data * t = allocate_timer_data();

	if (t)
	{
		t->file_descriptor = timerfd_create(CLOCK_MONOTONIC, 0);

		if (t->file_descriptor > 0)
		{
			struct itimerspec ii =
			{
				.it_interval = *i,
				.it_value = *d
			};

			if (!timerfd_settime(t->file_descriptor, 0, &ii, NULL))
			{
				t->priority = shift_priority(pr);
				t->triggered = false;
				t->expired = false;
				t->callback.user_data = a;
				t->callback.function = cb;

				schedule_timer(t);

				debug("Created a new timer (%d).\n", t->file_descriptor);
				return t;
			}
			close(t->file_descriptor);
		}
		free_timer_data(t);
	}
	return NULL;
}

static void
io_mark_pending_trigger(struct io_data * CALLBACKER_NONNULL io)
{
	assert(io);

	atomic_store(&io->triggered, true);
}

void
callbacker_io_trigger(callbacker_io_id_t id)
{
	assert(id);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);
	assert_io_data_ptr(id);

	struct io_data * io = (struct io_data *)id;

	io_mark_pending_trigger(io);
	try_trigger_io(io, CALLBACKER_IO_TYPE_TRIGGER);
}

void
callbacker_timer_trigger(callbacker_timer_id_t id)
{
	assert(id);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);
	assert_timer_data_ptr(id);

	struct timer_data * t = (struct timer_data *)id;
	uint64_t one = 1;

	atomic_store(&t->triggered, true);

	if (ioctl(t->file_descriptor, TFD_IOC_SET_TICKS, &one) == -1)
	{
		error("Failed to adjust the tick count of a timerfd.");
		abort();
	}
}

static void
_run_resolvable(void * args)
{
	assert(args);

	struct callbacker_retval * rv = (struct callbacker_retval *)args;

	rv->ptr.rv = rv->fn(rv->ptr.arg);
	lock_acquire(&rv->lock);
	condition_notify_all(&rv->cond);
	atomic_store(&rv->done, true);
	lock_release(&rv->lock);
}

bool
callbacker_run_resolvable(struct callbacker_retval * CALLBACKER_NONNULL rv,
                         int8_t pr,
                         void * CALLBACKER_NULLABLE (* CALLBACKER_NONNULL fn) (void * CALLBACKER_NULLABLE),
                         void * CALLBACKER_NULLABLE arg)
{
	assert(rv);
	assert(fn);

	if (lock_initialize(&rv->lock))
		return true;

	if (condition_initialize(&rv->cond, &rv->lock))
	{
		lock_uninitialize(&rv->lock);
		return true;
	}

	rv->fn = fn;
	rv->ptr.arg = arg;
	atomic_init(&rv->done, false);
	atomic_debug_flags_initialize(rv);

	if (callbacker_run(pr, _run_resolvable, rv))
	{
		condition_uninitialize(&rv->cond);
		lock_uninitialize(&rv->lock);
		return true;
	}

	atomic_debug_flags_set(rv, CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_PENDING);

	return false;
}

void *
callbacker_resolve(struct callbacker_retval * CALLBACKER_NONNULL rv)
{
	assert(rv);
	atomic_debug_flags_assert(rv, CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_PENDING);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	lock_acquire(&rv->lock);

	while (!atomic_load(&rv->done))
		condition_wait(&rv->cond);

	lock_release(&rv->lock);

	condition_uninitialize(&rv->cond);
	lock_uninitialize(&rv->lock);
	atomic_debug_flags_clear(rv, CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_PENDING);

	return rv->ptr.rv;
}

bool
callbacker_resolve_try(struct callbacker_retval * CALLBACKER_NONNULL rv,
                       void ** CALLBACKER_NONNULL arv)
{
	assert(rv);
	assert(arv);
	atomic_debug_flags_assert(rv, CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_PENDING);
	atomic_debug_flags_assert(&callbacker, CALLBACKER_ATOMIC_DEBUG_FLAG_INITIALIZED);

	if (atomic_load(&rv->done))
	{
		lock_acquire(&rv->lock);
		condition_uninitialize(&rv->cond);
		lock_release(&rv->lock);
		lock_uninitialize(&rv->lock);
		*arv = rv->ptr.rv;
		atomic_debug_flags_clear(rv, CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_PENDING);
		return false;
	}

	return true;
}
