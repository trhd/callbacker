#!/bin/bash

# callbacker -- A multithreaded callback queue / event loop library.
# Copyright (C) 2018-2019 Hemmo Nieminen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

waitpid()
{
	local pid="$1"

	if [ "$MODE" == "glitch" ]
	then
		! wait "$pid"
	else
		wait "$pid"
	fi
}

LOG_FILE="$(mktemp)"
trap 'rm -f $LOG_FILE' EXIT
trap 'echo ERROR on line $LINENO' ERR

MODE="$1"
SERVER="$2"
CLIENT="$3"

"$SERVER" --mode "$MODE" &>"$LOG_FILE" &
SERVER_PID=$!

for ((i=0 ; i<3 ; i=i+1))
do
	PORT="$(grep -E '^port: [0-9]+$' "$LOG_FILE" | grep -oE '[0-9]+' || :)"

	[ -n "$PORT" ] && break
done

if [ -z "$PORT" ]
then
	cat "$LOG_FILE"
	exit 99
fi

PIDS=()
NPROC="$(grep -c ^processor /proc/cpuinfo)"
[ -v VALGRIND ] && [ "$VALGRIND" != 0 ] && COUNT=$((2*NPROC)) || COUNT=$((10*NPROC))

for ((i=0; i < "$COUNT" ; i++))
do
	"$CLIENT" "$PORT" 1000 >/dev/null &
	PIDS+=($!)
done

for pid in "${PIDS[@]}"
do
	waitpid "$pid"
done

kill -SIGINT "$SERVER_PID"
wait "$SERVER_PID"
