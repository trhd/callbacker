/**
 * callbacker -- A multithreaded callback queue / event loop library.
 * Copyright (C) 2018-2019 Hemmo Nieminen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <assert.h>
#include <errno.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "callbacker_config.h"

static int
workloop(int sock, FILE * input, int count)
{
	char sndbuf[1000], rcvbuf[1000], * sndbufptr = sndbuf;
	size_t linelen = sizeof(sndbuf);
	int datalen, rv = EXIT_FAILURE;

	while ((datalen = fread(sndbufptr, 1, linelen, input)) > 0 && count --> 0)
	{
		for (int s = 0, r = 0 ; s < datalen || r < datalen ; )
		{
			if (s < datalen)
			{
				int l = send(sock, sndbufptr + s, datalen - s, 0);

				if (l < 0)
				{
					if (errno == EINTR)
						rv = EXIT_SUCCESS;
					else
						printf("\nFailed to send the the input to the server.");
					break;
				}

				s += l;
			}

			if (r < datalen)
			{
				int l = recv(sock, rcvbuf + r, sizeof(rcvbuf) - r, 0);
				if (l < 0)
				{
					if (errno == EINTR)
						rv = EXIT_SUCCESS;
					else
						printf("\nFailed to receive the input from the server: %s.", strerror(errno));
					break;
				}

				r += l;
			}
		}

		if (memcmp(sndbufptr, rcvbuf, datalen))
		{
			printf("\nThe received data doesn't match the sent data.");
			break;
		}

		if (count % 10 == 0)
		{
			printf(".");
			fflush(stdout);
		}
	}

	if (count == -1)
		rv = EXIT_SUCCESS;

	printf("\n");

	return rv;
}

int
main(int ac, char ** av)
{
	int sock, rv = EXIT_FAILURE;
	struct sockaddr_in sa;
	FILE * urand;

	if (ac != 3)
		fprintf(stderr, "Usage: %s PORT ITERATION_COUNT\n", av[0]);
	else
	{
		memset(&sa, 0, sizeof(sa));
		sa.sin_family = AF_INET;
		sa.sin_port = htons(atoi(av[1]));

		if ((sock = socket(AF_INET, SOCK_STREAM, 0)) > 0)
		{
			if ((urand = fopen("/dev/urandom", "r")))
			{

				if (!connect(sock, (struct sockaddr *)&sa, sizeof(sa)))
					rv = workloop(sock, urand, atoi(av[2]));
				fclose(urand);
			}
			close(sock);
		}
	}

	return rv;
}
