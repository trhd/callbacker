#!/bin/bash

# callbacker -- A multithreaded callback queue / event loop library.
# Copyright (C) 2018-2019 Hemmo Nieminen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -e

ignore_list=( "DEBUG_FLAGS" "FLAGS" )
rv=0

srcdir="$1"
req="$2"
tstdir="$3"
cov="$4"
mesondefs="$5"

ctags -x --c-kinds=pf $(find "$srcdir" -type f -regex '^.*\.h\(\.in\)?') \
	| cut -f 1 -d ' ' \
	| grep -vE "$( IFS=\| ; echo "${ignore_list[*]}" )" >"$req"

ctags -x --c-kinds=f $(find "$tstdir" -type f -regex '^.*\.c\(\.in\)?') \
	| cut -f 1 -d ' ' \
	| grep -E '^UT_' >"$cov"

while read -r fn
do
	grep -qE "^UT_$fn(__.*)?" "$cov" \
		&& printf "%15s: %s\n" "implemented" "$fn" \
		|| { rv=$(($rv + 1)) ; printf "%15s: %s\n" "NOT IMPLEMENTED" "$fn" ; }
done < "$req"

while read -r fn
do
	grep -q "'$fn'" "$mesondefs" \
		&& printf "%15s: %s\n" "executed" "$fn" \
		|| { rv=$(($rv + 1)) ; printf "%15s: %s\n" "NOT EXECUTED" "$fn" ; }
done < <(grep -oE 'cmocka_unit_test[^(]*\([^)]+\)' "$tstdir"/unit_tests.c | sed -e 's/^[^(]\+(//' -e 's/)$//')

exit $rv
