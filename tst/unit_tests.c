/**
 * callbacker -- A multithreaded callback queue / event loop library.
 * Copyright (C) 2018-2019 Hemmo Nieminen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "callbacker.h"
#include "cmocka-wrapper.h"
#include "dsac/lock.h"

#include <dsac/condition.h>
#include <dsac/lock.h>
#include <errno.h>
#include <fcntl.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <time.h>
#include <unistd.h>

#define A_LONG_ASS_STRING \
	"löleaöiojöslagsdilsskajfdhösaldfsifjeosifjseFOSDAFIJASosoaijdflsdkjF" \
	"kfhLASKHIESLGHdjfi897djhSLEIAHNGDLKFHLSAKDJFLIESHNFGLSADKJNGseTGsaed" \
	"löleaöiojöslagsdskaffjafdhösaldfsifjeosifjseFOSDAFIJASosoaijdflsdkjF" \
	"adjhfhLASKHIESlskdlsi897LGHSLEIAHNGDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"löleaöiojöslagsdskajdliedhösaldfsifjeosifjseFOSDAFIJASosoaijdflsdkjF" \
	"adjhfhLASKHIESlskdjfs897LGHSLEIAHNGDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"l9eaöeiojöslagsdskajfölidhösaldfsifjeosifjseFOSDAFIJASosoaijdflsdkjF" \
	"adjhfhLASKHIESSLEIAHNGDLKFHlskdjfls87LGHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"ld9deöiojöslagsaldfsifjeosisdskajföledhöfjseFOSDAFIJASosoaijdflsdkjF" \
	"a7jhfhLASKHIESSLEIAHNGDLKFHlskdjflsi8LGHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"laöidhojösl agsaldfsifjeosisdskajföledhöfjseFOSDAFIJASosoaijdflsdkjF" \
	"a97jfhLASKHIESSLEIAHNGDLKFHlskdjflsi8LGHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"laöio hhdjslagsaldfsifjeosisdskajföledhöfjseFOSDAFIJASosoaijdflsdkjF" \
	"a9udjfLIAHNGDLKFHlASKHIESSLEskdjflsi8LGHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"laöiojödfsifjeosisfLslaassaldskajföledhöfjseFOSDAFIJASosoaijdflsdkjF" \
	"a7udjhhIAHNGDLKFHlASKHIESSLEskdjflsi8LGHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajdfsifjeosioföleaöifaljöslLSagdhsdfjseFOSDAFIJASosoaijdflsdkjF" \
	"alskdjfIAHNGDLKFHjlsi87udSLEhfhAKHIESLGHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajifsifjeosioföleaöialdjöslagSHdhösfjseFOSDAFIJASosoaijdflsdkjF" \
	"alskdjfIAHNGDLKFHjlsi87udSLEhfhLAKIESLGHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajföleaöialdfsifijeosiojöslagdhHEösfjseFOSDAFIJASosoaijdflsdkjF" \
	"alskdjflsi97udjhfhLASKISLGHSLEIAHNGDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajföleaöiojöslagdhösEaldfsifjeosi3fjseFOSDAFIJASosoaijdflsdkjFL" \
	"alskdjflsi897djhfhLASKHISLGHSLEIAHNGDLKFHLSAKDJFLIESHNFGLSADKJNGseTG" \
	"lsdskajföleaöiojöslagdhösaSGldfsifjeosifjse3FOSDAFIJASosoaijdflsdkjF" \
	"alskdjflsi897djhfhLASKHIELHSLEIAHNGDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajföleaöiojöslagdhösaldGSfsifjeosifjseFOS5DAFIJASosoaijdflsdkjF" \
	"alskdjflsi897djhfhLASKHIESLHLEIAHNGDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajföleaöiojöslagdhösaldfsSEifjeosifjseFOSDAFI8JASosoaijdflsdkjF" \
	"alskdjflsi897LGHLIAHNGdjhfhLASKHIESDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajföleaöldfsifEAjiojöslagdhösaeosifjseFOSDAFIJASo9soaijdflsdkjF" \
	"alskdjflsi897LGHSLIHNGdjhHSLASKHIESDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"lsdskajföleaöldfsifjeAiojhalagdhösaNosifjseFOSDAFIJASosoa6ijdflsdkjF" \
	"lsi897djhfhLALGHSLEIHGdjhHSjfSKHIESDLKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"föleaöiojöslaldfsifjeoiojhaajgdhösasNifjseFOSDAFIJASosoaijd0flsdkjFL" \
	"lsi897djhfhLALGHSLEIAHalsHSjfSKHIESGDLKFHLSAKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslaldfsifjeolsdhaajgdhösasifjseFOSDAAFIJASosoaijdflsxdkjFL" \
	"lsi897djhfhLALGHSLEIAHalsHSjfSKHIESNGDLKFHLSKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslalsdskajgdlsdhadfsifjeosifjseFOSSDAFIJASosoaijdflsdkjFdl" \
	"lsi897djhfhLAalskdjfSKalsHSGHSLEIAHNGDLKFHLAKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslalsdskajgdldfhösasifjeosifjseFOLSDAFIJASosoaijdflsdkjFLa" \
	"lsi897djhfhLAalskdjfSKLGHHIESSLEIAHNGDLKFHSAKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslalsdskajgdldfhösasifjeosifjseFHOSDAFIJASosoaijdflsdkjFLa" \
	"lsi897djhfhLAl8skdjfSKLGHHIESSLEIAHNGDLKFLSAKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslalsdskajgdldfhösasifjeosifjseFFOSDAFIJASosoaijdflsdkjFLa" \
	"lsi897djhfhLAlskdj8fSKLGHHIESSLEIAHNGDLKHLSAKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslalsdskajgdldfhösasifjeosifjsKeFOSDAFIJASosoaijdflsdkjFLa" \
	"sia8977jhfhLAlskdjflSKLGHHIESSLEIAHNGDLFHLSAKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslalsdskajgdldfhösasifjeosifjLseFOSDAFIJASosoaijdflsdkjFLa" \
	"sia897jhfhL(AlskdjflSKLGHHIESSLEIAHNGDKFHLSAKDJFLIESHNFGLSADKJNGseTG" \
	"föleaöiojöslalsdskajgdldfhösasifjeosiGLfjseFOSDAFIJASosoaijdflsdkjFL" \
	"lskdjflsia897jhfhLASKHLGHEIESSLEIAHNDKFHLSAKDJFLIESHNFGLSADKJNGseTGs" \
	"faaissiFLaiswiefjaedfaaissiFLwiefjaedfasiFLaiswiefjaedfasiFLaiswiefj" \
	"aedfasiFLaiswiefjaedfaaissiFLwiefjaedfasiFLaiswiefjaedfasiFLaiswiefj" \
	"aedfasiFLaiswiefjaedfaaissiFLwiefjaedfasiFaiswiefjsaedfasiFLaiswiefj" \
	"aedfasiFLaiswiefjaedfasiFLaiswiefjaedfasiFLaiswiefjaedfasiFLaiswiefj" \
	"aedfasiFaiswiefjsaedfasiFaiswiefjsaedfasiFLiswiefjsaedfasiFiswieafjs" \
	"aedfasiFiswiefjasaedfasiFiswiefjasaedfasiFiswiefjasaedfasiFiswiefjas" \
	"aaaasdkfjiefaiswiefjaaedfasiFLwiefjalsMAX-MACRO-LEN"

static bool VALGRIND = false;

static void * SNEAKY_NULL = NULL;
static void (* SNEAKY_NULL_DIRECT) (void *) = NULL;
static void * (* SNEAKY_NULL_DIRECT_RESOLVABLE) (void *) = NULL;
static enum callbacker_io_type (* SNEAKY_NULL_IO) (int, enum callbacker_io_type, void *) = NULL;
static void (* SNEAKY_NULL_SIGNAL) (int, void *) = NULL;
static enum callbacker_timer_type (*SNEAKY_NULL_TIMER)(callbacker_timer_id_t, enum callbacker_timer_type, void *) = NULL;

static struct timespec half_sec = { 0, 500*1000*1000 };

struct holder
{
	struct lock lock;
	struct condition cond;
	int error;
	void * arg;
	atomic_int i;
	atomic_int j;
	atomic_int k;
	union
	{
		struct
		{
			char buffer[1024];
			int buffer_usage;
		};
		atomic_int signals[64];
	};
};

static void
holder_initialize(struct holder * CALLBACKER_NONNULL h)
{
	assert(h);

	memset(h, 0, sizeof(*h));

	assert_false(lock_initialize(&h->lock));
	assert_false(condition_initialize(&h->cond, &h->lock));
	atomic_init(&h->i, 0);
	atomic_init(&h->j, 0);
	atomic_init(&h->k, 0);
}

static void
holder_uninitialize(struct holder * CALLBACKER_NONNULL h)
{
	assert(h);

	condition_uninitialize(&h->cond);
	lock_uninitialize(&h->lock);
}

static void
reasonable_sleep(void)
{
	struct timespec delay = { 0, 20 * 1000 * 1000 };
	char const * V = getenv("VALGRIND");

	if (V && atoi(V))
		delay.tv_sec = 1;

	assert_int_equal(0, nanosleep(&delay, NULL));
}

static void
_stop(void * p __attribute__((unused)))
{
	assert_false(callbacker_threads_count_set(0));
}

static void
mask_signal(int s)
{
	assert_true(s);

	sigset_t ss;

	sigemptyset(&ss);
	sigaddset(&ss, s);

	assert_false(sigprocmask(SIG_BLOCK, &ss, NULL));
}

static void
unmask_signal(int s)
{
	assert_true(s);

	sigset_t ss;

	sigemptyset(&ss);
	sigaddset(&ss, s);

	assert_false(sigprocmask(SIG_UNBLOCK, &ss, NULL));
}

static void *
resolvable_dummy_sleeper(void * user_data)
{
	reasonable_sleep();
	return (void*)(((unsigned long)user_data) * 2);
}

static void
dummy_sleeper(void * user_data CALLBACKER_UNUSED)
{
	assert(signal);
	reasonable_sleep();
}

static void
dummy_sleeper__long(void * user_data CALLBACKER_UNUSED)
{
	assert(signal);

	for (int i = 0 ; i < 5 ; i++)
		reasonable_sleep();
}

static void
dummy_abort(void * user_data CALLBACKER_UNUSED)
{
	abort();
}

static void
dummy_signal_handler(int signal, void * user_data CALLBACKER_UNUSED)
{
	assert(signal);
}

static enum callbacker_io_type
dummy_io_handler(int fd, enum callbacker_io_type type CALLBACKER_UNUSED, void * user_data CALLBACKER_UNUSED)
{
	assert(fd > 0);

	return CALLBACKER_IO_TYPE_NONE;
}

static enum callbacker_timer_type
dummy_timer_handler(callbacker_timer_id_t id, enum callbacker_timer_type t, void * user_data CALLBACKER_UNUSED)
{
	assert(id);
	assert(t);

	return CALLBACKER_TIMER_TYPE_EXPIRATION;
}

static enum callbacker_io_type
dummy_io_abort(int fd CALLBACKER_UNUSED, enum callbacker_io_type type, void * user_data CALLBACKER_UNUSED)
{
	if (type & CALLBACKER_IO_TYPE_TRIGGER)
		return CALLBACKER_IO_TYPE_NONE;

	abort();
}

enum callbacker_io_type
echo_server(int fd, enum callbacker_io_type type, void * p CALLBACKER_UNUSED)
{
	assert(fd);

	char buf;

	if (type & CALLBACKER_IO_TYPE_READ)
	{
		if (read(fd, &buf, 1) == 1)
			write(fd, &buf, 1);

		return CALLBACKER_IO_TYPE_READ;
	}

	return CALLBACKER_IO_TYPE_NONE;
}

/***********************************************************************/

static void
UT_callbacker_initialize()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);

	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_int_equal(callbacker_threads_count_get(), 0);
	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_initialize__NULL()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	bool dummy CALLBACKER_UNUSED;

	assert_non_null(buf);

	expect_assert_failure((dummy = callbacker_initialize(SNEAKY_NULL, 0)));
	expect_assert_failure((dummy = callbacker_initialize(buf, 0)));
	expect_assert_failure((dummy = callbacker_initialize(SNEAKY_NULL, buf_size)));

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_initialize__twice()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size),
	     * buf2 = test_malloc(buf_size);
	bool dummy CALLBACKER_UNUSED;

	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	expect_assert_failure((dummy = callbacker_initialize(buf2, buf_size)));
	callbacker_uninitialize();

	test_free(buf);
	test_free(buf2);
}

/***********************************************************************/

static void
UT_callbacker_uninitialize()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(2));
	assert_false(callbacker_threads_count_set(0));
	callbacker_uninitialize();

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));
	assert_false(callbacker_threads_start(1));
	assert_false(callbacker_threads_start(1));
	assert_false(callbacker_threads_count_set(0));
	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_uninitialize__uninitialized()
{
	enum { buf_size = 200 };

	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();
	expect_assert_failure(callbacker_uninitialize());

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_uninitialize__running()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(2));
	callbacker_uninitialize();

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));
	assert_false(callbacker_threads_start(1));
	assert_false(callbacker_threads_start(1));
	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_count_set()
{
	enum { buf_size = 200 };

	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));

	assert_int_equal(callbacker_threads_count_get(), 0);

	assert_false(callbacker_threads_count_set(5));
	assert_int_equal(callbacker_threads_count_get(), 5);

	assert_false(callbacker_threads_count_set(2));
	assert_int_equal(callbacker_threads_count_get(), 2);

	assert_false(callbacker_threads_count_set(1));
	assert_int_equal(callbacker_threads_count_get(), 1);

	assert_false(callbacker_threads_count_set(6));
	assert_int_equal(callbacker_threads_count_get(), 6);

	assert_false(callbacker_threads_count_set(0));
	assert_int_equal(callbacker_threads_count_get(), 0);

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_count_set__uninitialized()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure((dummy = callbacker_threads_count_set(2)));

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_count_get()
{
	enum { buf_size = 200 };

	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));

	assert_int_equal(callbacker_threads_count_get(), 0);

	assert_false(callbacker_threads_count_set(5));
	assert_int_equal(callbacker_threads_count_get(), 5);

	assert_false(callbacker_threads_count_set(2));
	assert_int_equal(callbacker_threads_count_get(), 2);

	assert_false(callbacker_threads_start(1));
	assert_int_equal(callbacker_threads_count_get(), 3);

	assert_false(callbacker_threads_count_set(1));
	assert_int_equal(callbacker_threads_count_get(), 1);

	assert_false(callbacker_threads_count_set(6));
	assert_int_equal(callbacker_threads_count_get(), 6);

	callbacker_threads_stop(3);
	assert_int_equal(callbacker_threads_count_get(), 3);

	callbacker_threads_stop(1);
	assert_int_equal(callbacker_threads_count_get(), 2);

	assert_false(callbacker_threads_count_set(0));
	assert_int_equal(callbacker_threads_count_get(), 0);

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_count_get__uninitialized()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure((dummy = callbacker_threads_count_get()));

	test_free(buf);
}

/***********************************************************************/

static void
_UT_callbacker_start_thread__helper__multi(void * p)
{
	assert(p);

	struct holder * h = (struct holder *)p;

	lock_acquire(&h->lock);

	atomic_fetch_add(&h->i, 1);
	atomic_fetch_add(&h->j, 1);

	while (atomic_load(&h->j) < 4)
		condition_wait(&h->cond);
	condition_broadcast(&h->cond);

	lock_release(&h->lock);
}

static void
_UT_callbacker_start_thread__helper(void * p)
{
	assert(p);

	struct holder * h = (struct holder *)p;

	lock_acquire(&h->lock);
	while (!atomic_load(&h->j))
		condition_wait(&h->cond);
	atomic_store(&h->j, 0);
	atomic_fetch_add(&h->i, 1);
	condition_signal(&h->cond);
	lock_release(&h->lock);
}

static void
UT_callbacker_threads_start()
{
	size_t const buf_size = 8 * callbacker_data_size();
	char * buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));

	assert_false(callbacker_threads_count_get());
	assert_false(callbacker_threads_start(1));
	assert_int_equal(callbacker_threads_count_get(), 1);
	assert_false(callbacker_threads_start(3));
	assert_int_equal(callbacker_threads_count_get(), 4);
	callbacker_threads_stop(4);
	assert_false(callbacker_threads_count_get());

	callbacker_uninitialize();
	test_free(buf);
}

static void
UT_callbacker_threads_start__scheduling()
{
	size_t const buf_size = 8 * callbacker_data_size();
	char * buf = test_malloc(buf_size);
	struct holder h;

	holder_initialize(&h);
	assert_false(callbacker_initialize(buf, buf_size));

	for (int i = 0 ; i < 3 ; i++)
		assert_false(callbacker_run(0, _UT_callbacker_start_thread__helper, &h));

	reasonable_sleep();
	assert_int_equal(h.i, 0);

	lock_acquire(&h.lock);

	assert_false(callbacker_threads_start(1));

	atomic_store(&h.j, 1);
	condition_signal(&h.cond);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 1);

	atomic_store(&h.j, 1);
	condition_signal(&h.cond);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 2);

	atomic_store(&h.j, 1);
	condition_signal(&h.cond);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 3);

	atomic_store(&h.j, 1);
	condition_signal(&h.cond);
	assert_true(condition_timedwait(&h.cond, &half_sec));

	assert_false(callbacker_threads_start(1));
	assert_false(callbacker_threads_start(2));

	atomic_store(&h.j, 0);
	for (int i = 0 ; i < 4 ; i++)
		assert_false(callbacker_run(0, _UT_callbacker_start_thread__helper__multi, &h));

	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 7);

	assert_false(callbacker_threads_count_set(0));

	lock_release(&h.lock);

	callbacker_uninitialize();
	holder_uninitialize(&h);

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_start__NULL()
{
	char * buf = test_malloc(100);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, 100));

	expect_assert_failure((dummy = callbacker_threads_start(0)));

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_start__uninitialized()
{
	char * buf = test_malloc(100);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, 100));
	callbacker_uninitialize();

	expect_assert_failure((dummy = callbacker_threads_start(1)));

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_stop()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));

	assert_false(callbacker_threads_count_get());
	assert_false(callbacker_threads_start(10));
	assert_int_equal(callbacker_threads_count_get(), 10);
	callbacker_threads_stop(4);
	assert_int_equal(callbacker_threads_count_get(), 6);
	callbacker_threads_stop(1);
	assert_int_equal(callbacker_threads_count_get(), 5);
	assert_false(callbacker_threads_count_set(0));
	assert_int_equal(callbacker_threads_count_get(), 0);
	assert_false(callbacker_threads_count_get());

	assert_false(callbacker_threads_start(4));
	assert_int_equal(callbacker_threads_count_get(), 4);
	callbacker_threads_stop(0);
	assert_int_equal(callbacker_threads_count_get(), 0);
	assert_false(callbacker_threads_start(4));
	assert_int_equal(callbacker_threads_count_get(), 4);
	callbacker_threads_stop(0);
	assert_int_equal(callbacker_threads_count_get(), 0);
	assert_false(callbacker_threads_start(4));
	assert_int_equal(callbacker_threads_count_get(), 4);
	callbacker_threads_stop(0);
	assert_int_equal(callbacker_threads_count_get(), 0);

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_stop__nonexisting()
{
	enum { buf_size = 100 };
	char * buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));
	callbacker_threads_stop(2);
	assert_int_equal(callbacker_threads_count_get(), 0);
	assert_false(callbacker_threads_start(1));
	reasonable_sleep();
	callbacker_threads_stop(2);
	assert_int_equal(callbacker_threads_count_get(), 0);
	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_stop__nonrunning()
{
	enum { buf_size = 100 };
	char * buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));

	callbacker_threads_stop(1);
	assert_int_equal(callbacker_threads_count_get(), 0);

	assert_false(callbacker_threads_start(2));
	callbacker_threads_stop(3);
	assert_int_equal(callbacker_threads_count_get(), 0);

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_threads_stop__uninitialized()
{
	enum { buf_size = 100 };
	char * buf = test_malloc(buf_size);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure(callbacker_threads_stop(1));

	test_free(buf);
}

/***********************************************************************/

atomic_int _UT_callbacker_run__counter;

static void
_UT_callbacker_run__test_leet_arg(void * p)
{
	assert_int_equal((unsigned long)p, 0x1337UL);

	atomic_fetch_add(&_UT_callbacker_run__counter, 1);
}

static void
_UT_callbacker_run__test_null_arg(void * p CALLBACKER_UNUSED)
{
	atomic_fetch_add(&_UT_callbacker_run__counter, 1);
}

static void
UT_callbacker_run()
{
	enum { buf_size = 400 };
	char * buf = test_malloc(buf_size);
	struct holder h;

	assert_false(callbacker_initialize(buf, buf_size));
	holder_initialize(&h);

	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, _stop, SNEAKY_NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY,
	                            _UT_callbacker_run__test_null_arg,
	                            SNEAKY_NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY,
	                            _UT_callbacker_run__test_leet_arg,
	                            (void*)0x1337UL));
	callbacker_exec();

	assert_int_equal(atomic_load(&_UT_callbacker_run__counter), 2);

	callbacker_uninitialize();
	holder_uninitialize(&h);
	test_free(buf);
}

/***********************************************************************/

atomic_uint _UT_callbacker_run__priorities__low_counter;
atomic_uint _UT_callbacker_run__priorities__default_counter;
atomic_uint _UT_callbacker_run__priorities__high_counter;

static void
_UT_callbacker_run__priorities__default(void * p)
{
	assert(!p);

	assert_int_equal(0, atomic_load(&_UT_callbacker_run__priorities__low_counter));
	assert_int_equal(1000, atomic_load(&_UT_callbacker_run__priorities__high_counter));

	atomic_fetch_add(&_UT_callbacker_run__priorities__default_counter, 1);
}

static void
_UT_callbacker_run__priorities__low(void * p)
{
	assert(!p);

	assert_int_equal(1000, atomic_load(&_UT_callbacker_run__priorities__default_counter));
	assert_int_equal(1000, atomic_load(&_UT_callbacker_run__priorities__high_counter));

	atomic_fetch_add(&_UT_callbacker_run__priorities__low_counter, 1);
}

static void
_UT_callbacker_run__priorities__high(void * p)
{
	assert(!p);

	assert_int_equal(0, atomic_load(&_UT_callbacker_run__priorities__low_counter));
	assert_int_equal(0, atomic_load(&_UT_callbacker_run__priorities__default_counter));

	atomic_fetch_add(&_UT_callbacker_run__priorities__high_counter, 1);
}

static void
_UT_callbacker_run__priorities__stop(void * p CALLBACKER_UNUSED)
{
	assert_false(callbacker_threads_count_set(0));
}

static void
UT_callbacker_run__priorities()
{
	size_t const buf_size = 3002 * callbacker_data_size();
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	atomic_init(&_UT_callbacker_run__priorities__default_counter, 0);
	atomic_init(&_UT_callbacker_run__priorities__high_counter, 0);
	atomic_init(&_UT_callbacker_run__priorities__low_counter, 0);

	assert_false(callbacker_initialize(buf, buf_size));

	for (int i = 0 ; i < 1000 ; i++)
		assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, _UT_callbacker_run__priorities__high, NULL));

#if CALLBACKER_FAIR_SCHEDULING != 1
	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, _UT_callbacker_run__priorities__stop, NULL));
#endif

	for (int i = 0 ; i < 1000 ; i++)
		assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, _UT_callbacker_run__priorities__low, NULL));

#if CALLBACKER_FAIR_SCHEDULING == 1
	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, _UT_callbacker_run__priorities__stop, NULL));
#endif

	for (int i = 0 ; i < 1000 ; i++)
		assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_run__priorities__default, NULL));

	callbacker_exec();

	assert_int_equal(1000, atomic_load(&_UT_callbacker_run__priorities__default_counter));
	assert_int_equal(1000, atomic_load(&_UT_callbacker_run__priorities__high_counter));
	assert_int_equal(1000, atomic_load(&_UT_callbacker_run__priorities__low_counter));

	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_run__NULL()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));

	expect_assert_failure((dummy = callbacker_run(CALLBACKER_DEFAULT_PRIORITY,
				SNEAKY_NULL_DIRECT, SNEAKY_NULL)));

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_run__memory()
{
	enum { ok_count = 5, iter_count = 10, fail_count = 3 };
	int buf_size = ok_count * callbacker_data_size();
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(4));
	errno = 0;

	for (int j = 0 ; j < iter_count ; j++)
	{
		for (int i = 0 ; i < ok_count ; i++)
			assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));

		for (int i = 0 ; i < fail_count ; i++)
		{
			assert_true(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
			assert_int_equal(errno, ENOMEM);
			errno = 0;
		}

		for (int i = 0 ; i < 5 ; i++)
			reasonable_sleep();
	}

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_run__uninitialized()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure((dummy = callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_abort, NULL)));

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_run__invalid_args()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));

	expect_assert_failure((dummy = callbacker_run(CALLBACKER_LOW_PRIORITY + 1, dummy_abort, NULL)));
	expect_assert_failure((dummy = callbacker_run(CALLBACKER_HIGH_PRIORITY - 1, dummy_abort, NULL)));

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static enum callbacker_io_type
_UT_callbacker_io_start__read__helper(int fd, enum callbacker_io_type type, void * arg)
{
	assert(fd);

	struct holder * h = (struct holder *)arg;
	enum callbacker_io_type rv = CALLBACKER_IO_TYPE_NONE;

	lock_acquire(&h->lock);

	if (type & CALLBACKER_IO_TYPE_ERROR)
		atomic_fetch_add(&h->i, 1);
	else if (type & CALLBACKER_IO_TYPE_READ)
	{
		h->buffer_usage = read(fd, &h->buffer, sizeof(h->buffer));
		if (h->buffer_usage < 0)
			++h->error;
		else
			rv = CALLBACKER_IO_TYPE_READ;
	}

	if (type & CALLBACKER_IO_TYPE_WRITE)
		++h->error;

	condition_signal(&h->cond);
	lock_release(&h->lock);

	return rv;
}

static void
_UT_callbacker_io_start__read__write_test(int fd, char const * data, struct holder * h)
{
	assert(data);
	assert(h);
	assert(fd >= 0);

	size_t z = strlen(data);
	lock_acquire(&h->lock);
	h->buffer[z] = (char)0xaa;
	assert_int_equal(write(fd, data, z), z);
	assert_false(condition_timedwait(&h->cond, &half_sec));
	assert_int_equal(h->error, 0);
	assert_memory_equal(data, h->buffer, z);
	assert_int_equal(h->buffer_usage, z);
	assert_int_equal((uint8_t)h->buffer[z], (uint8_t)0xaa);
	lock_release(&h->lock);
}

static void
UT_callbacker_io_start__read()
{
	size_t const buf_size = 6 * callbacker_data_size();
	char * buf, * tmpbuf;
	int pipefd[2], error = 0;
	struct holder h;

	holder_initialize(&h);
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));
	buf = test_malloc(buf_size);
	tmpbuf = test_malloc(100);
	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(fcntl(pipefd[0], F_SETFL, O_NONBLOCK));

	assert_false(callbacker_threads_start(1));

	assert_non_null(callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_READ,
				CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_io_start__read__helper, &h));

	assert_false(callbacker_threads_start(1));

	assert_int_equal(read(pipefd[0], tmpbuf, 100), -1);
	assert_int_equal(errno, EWOULDBLOCK);

	_UT_callbacker_io_start__read__write_test(pipefd[1], "kekeke", &h);
	assert_int_equal(read(pipefd[0], tmpbuf, 100), -1);
	assert_int_equal(errno, EWOULDBLOCK);

	_UT_callbacker_io_start__read__write_test(pipefd[1], "moi", &h);
	assert_int_equal(read(pipefd[0], tmpbuf, 100), -1);
	assert_int_equal(errno, EWOULDBLOCK);

	_UT_callbacker_io_start__read__write_test(pipefd[1], "tsaukki", &h);
	assert_int_equal(read(pipefd[0], tmpbuf, 100), -1);
	assert_int_equal(errno, EWOULDBLOCK);

	lock_acquire(&h.lock);

	assert_int_equal(atomic_load(&h.i), 0);
	close(pipefd[1]);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 1);
	assert_true(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 1);

	lock_release(&h.lock);

	assert_false(callbacker_threads_count_set(0));
	callbacker_uninitialize();

	close(pipefd[0]);
	close(pipefd[1]);

	assert_int_equal(error, 0);

	holder_uninitialize(&h);
	test_free(buf);
	test_free(tmpbuf);
}

/***********************************************************************/

static atomic_uint _UT_callbacker_io_start__write__counter;

static enum callbacker_io_type
_UT_callbacker_io_start__write__helper(int fd, enum callbacker_io_type type, void * arg)
{
	assert(fd >= 0);
	assert(arg);

	struct holder * h = (struct holder *)arg;
	enum callbacker_io_type rv = CALLBACKER_IO_TYPE_WRITE;

	lock_acquire(&h->lock);

	if (type & CALLBACKER_IO_TYPE_WRITE)
	{
		bool gotit = false;

		while (write(fd, "x", 1) == 1)
		{
			atomic_fetch_add(&h->i, 1);
			gotit = true;
		}

		if (!gotit)
			++h->error;
	}

	if (type & CALLBACKER_IO_TYPE_READ)
		++h->error;

	if (type & CALLBACKER_IO_TYPE_ERROR)
	{
		atomic_fetch_add(&h->j, 1);
		rv = CALLBACKER_IO_TYPE_NONE;
	}

	condition_signal(&h->cond);
	lock_release(&h->lock);

	return rv;
}

static void
_UT_callbacker_io_start__write__read(int fd, unsigned int c, struct holder * h)
{
	assert(fd > 0);
	assert(h);

	char buf;

	atomic_store(&h->i, 0);

	for (unsigned int i = 0 ; i < c ; i++)
	{
		assert_int_equal(read(fd, &buf, 1), 1);
		assert_false(condition_timedwait(&h->cond, &half_sec));
	}

	assert_int_equal(atomic_load(&h->i), c);
}

static void
UT_callbacker_io_start__write()
{
	size_t const buf_size = 6 * callbacker_data_size();
	char * buf;
	int pipefd[2];
	struct holder h;

	holder_initialize(&h);
	pipe2(pipefd, O_DIRECT | O_NONBLOCK);
	buf = test_malloc(buf_size);
	assert_non_null(buf);
	atomic_init(&_UT_callbacker_io_start__write__counter, 0);
	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));

	lock_acquire(&h.lock);

	assert_non_null(callbacker_io_start(pipefd[1],
	                                    CALLBACKER_IO_TYPE_WRITE,
	                                    CALLBACKER_DEFAULT_PRIORITY,
	                                    _UT_callbacker_io_start__write__helper,
	                                    &h));

	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_true(atomic_load(&h.i) > 0);

	_UT_callbacker_io_start__write__read(pipefd[0], 47, &h);
	_UT_callbacker_io_start__write__read(pipefd[0], 10, &h);
	_UT_callbacker_io_start__write__read(pipefd[0], 0, &h);
	_UT_callbacker_io_start__write__read(pipefd[0], 100, &h);

	assert_int_equal(atomic_load(&h.j), 0);
	close(pipefd[0]);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.j), 1);
	assert_false(h.error);

	lock_release(&h.lock);

	callbacker_uninitialize();
	holder_uninitialize(&h);

	close(pipefd[1]);

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_io_start__no_threads()
{
	size_t const buf_size = 6 * callbacker_data_size();
	char * buf, * tmpbuf;
	int pipefd[2];

	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));

	buf = test_malloc(buf_size);
	tmpbuf = test_malloc(100);

	assert_false(callbacker_initialize(buf, buf_size));

	assert_non_null(callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_READ,
				CALLBACKER_DEFAULT_PRIORITY, dummy_io_abort, NULL));

	assert_int_equal(write(pipefd[1], "FOOBAR", 6), 6);
	reasonable_sleep();
	assert_int_equal(read(pipefd[0], tmpbuf, 100), 6);
	assert_memory_equal("FOOBAR", tmpbuf, 6);

	close(pipefd[1]);
	close(pipefd[0]);

	callbacker_uninitialize();

	test_free(buf);
	test_free(tmpbuf);
}

/***********************************************************************/

static void
UT_callbacker_io_start__memory()
{
	size_t const buf_size = 3 * callbacker_data_size();
	char * buf;
	int pipefd[4][2];
	void * subs[3];

	for (int i = 0 ; i < 4 ; i++)
		assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd[i]));

	buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(2));

	for (int i = 0 ; i < 3 ; i++)
	{
		subs[i] = callbacker_io_start(pipefd[i][0],
		                              CALLBACKER_IO_TYPE_READ,
		                              CALLBACKER_DEFAULT_PRIORITY,
		                              dummy_io_abort,
		                              NULL);
		assert_non_null(subs[i]);
	}

	assert_null(callbacker_io_start(pipefd[3][0],
	                                CALLBACKER_IO_TYPE_READ,
	                                CALLBACKER_DEFAULT_PRIORITY,
	                                dummy_io_abort,
	                                NULL));
	assert_int_equal(errno, ENOMEM);
	errno = 0;

	assert_null(callbacker_io_start(pipefd[3][0],
	                                CALLBACKER_IO_TYPE_READ,
	                                CALLBACKER_DEFAULT_PRIORITY,
	                                dummy_io_abort,
	                                NULL));
	assert_int_equal(errno, ENOMEM);
	errno = 0;

	for (int i = 0 ; i < 3 ; i++)
	{
		callbacker_io_trigger(subs[i]);

		do
		{
			subs[i] = callbacker_io_start(pipefd[i][0],
			                              CALLBACKER_IO_TYPE_READ,
			                              CALLBACKER_DEFAULT_PRIORITY,
			                              dummy_io_abort,
			                              NULL);
		}
		while (!subs[i]);
	}

	assert_null(callbacker_io_start(pipefd[3][0],
	                                CALLBACKER_IO_TYPE_READ,
	                                CALLBACKER_DEFAULT_PRIORITY,
	                                dummy_io_abort,
	                                NULL));
	assert_int_equal(errno, ENOMEM);
	errno = 0;

	for (int i = 0 ; i < 3 ; i++)
		callbacker_io_trigger(subs[i]);

	for (int i = 0 ; i < 3 ; i++)
	{
		do
		{
			subs[i] = callbacker_io_start(pipefd[i][0],
			                              CALLBACKER_IO_TYPE_READ,
			                              CALLBACKER_DEFAULT_PRIORITY,
			                              dummy_io_abort,
			                              NULL);
		}
		while (!subs[i]);
	}

	callbacker_uninitialize();

	for (int i = 0 ; i < 4 ; i++)
	{
		close(pipefd[i][0]);
		close(pipefd[i][1]);
	}

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_io_start__NULL()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	void * dummy CALLBACKER_UNUSED;
	int pipefd[2];


	assert_false(pipe(pipefd));
	assert_false(callbacker_initialize(buf, buf_size));

	expect_assert_failure((dummy = callbacker_io_start(0, CALLBACKER_IO_TYPE_NONE, 0, SNEAKY_NULL_IO, SNEAKY_NULL)));
	expect_assert_failure((dummy = callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_NONE, 0, SNEAKY_NULL_IO, SNEAKY_NULL)));
	expect_assert_failure((dummy = callbacker_io_start(0, CALLBACKER_IO_TYPE_READ, 0, SNEAKY_NULL_IO, SNEAKY_NULL)));
	expect_assert_failure((dummy = callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_READ, 0, SNEAKY_NULL_IO, SNEAKY_NULL)));
	expect_assert_failure((dummy = callbacker_io_start(0, CALLBACKER_IO_TYPE_NONE, 0, dummy_io_handler, SNEAKY_NULL)));
	expect_assert_failure((dummy = callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_NONE, 0, dummy_io_handler, SNEAKY_NULL)));
	expect_assert_failure((dummy = callbacker_io_start(0, CALLBACKER_IO_TYPE_READ, 0, dummy_io_handler, SNEAKY_NULL)));
	assert_non_null(callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_READ, 0, dummy_io_handler, SNEAKY_NULL));

	callbacker_uninitialize();

	close(pipefd[0]);
	close(pipefd[1]);

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_io_start__uninitialized()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	void * handle;
	int pipefd[2];

	assert_false(pipe(pipefd));
	assert_false(callbacker_initialize(buf, buf_size));

	handle = callbacker_io_start(pipefd[0],
	                             CALLBACKER_IO_TYPE_READ,
	                             CALLBACKER_DEFAULT_PRIORITY,
	                             dummy_io_handler,
	                             NULL);
	assert_non_null(handle);

	callbacker_io_trigger(handle);
	reasonable_sleep();

	callbacker_uninitialize();

	expect_assert_failure((handle = callbacker_io_start(pipefd[0],
	                                                   CALLBACKER_IO_TYPE_READ,
	                                                   CALLBACKER_DEFAULT_PRIORITY,
	                                                   dummy_io_handler,
	                                                   NULL)));

	close(pipefd[0]);
	close(pipefd[1]);

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_io_start__invalid_args()
{
	enum { buf_size = 200 };
	int pipefd[2];
	bool dummy CALLBACKER_UNUSED;
	char * buf = test_malloc(buf_size);

	assert_non_null(buf);
	assert_false(pipe(pipefd));

	assert_false(callbacker_initialize(buf, buf_size));

	expect_assert_failure((dummy = callbacker_io_start(pipefd[0],
	                                                   CALLBACKER_IO_TYPE_READ,
	                                                   CALLBACKER_LOW_PRIORITY + 1,
	                                                   dummy_io_handler,
	                                                   NULL)));

	expect_assert_failure((dummy = callbacker_io_start(pipefd[0],
	                                                   CALLBACKER_IO_TYPE_READ,
	                                                   CALLBACKER_HIGH_PRIORITY - 1,
	                                                   dummy_io_handler,
	                                                   NULL)));

	expect_assert_failure((dummy = callbacker_io_start(pipefd[0],
	                                                   CALLBACKER_IO_TYPE_NONE,
	                                                   CALLBACKER_HIGH_PRIORITY,
	                                                   dummy_io_handler,
	                                                   NULL)));

	expect_assert_failure((dummy = callbacker_io_start(pipefd[0],
	                                                   CALLBACKER_IO_TYPE_ERROR,
	                                                   CALLBACKER_HIGH_PRIORITY,
	                                                   dummy_io_handler,
	                                                   NULL)));

	expect_assert_failure((dummy = callbacker_io_start(-1,
	                                                   CALLBACKER_IO_TYPE_READ,
	                                                   CALLBACKER_HIGH_PRIORITY,
	                                                   dummy_io_handler,
	                                                   NULL)));

	callbacker_uninitialize();

	close(pipefd[0]);
	close(pipefd[1]);

	test_free(buf);
}

/***********************************************************************/

static enum callbacker_io_type
_UT_callbacker_io_start__start_twice__helper_1(int fd,
                                               enum callbacker_io_type CALLBACKER_UNUSED type,
                                               void * CALLBACKER_NONNULL ud)
{
	assert(fd >= 0);
	assert(ud);

	char buf[100];
	int t;
	atomic_int * i = (atomic_int *)ud;

	t = read(fd, buf, sizeof(buf));
	if (t != 6)
	{
		printf("%s: Expected t to be 6. Was %d.\n", __func__, t);
		abort();
	}

	for (int i = 0 ; i < 10 ; i++)
		reasonable_sleep();

	atomic_fetch_add(i, 1);

	return CALLBACKER_IO_TYPE_NONE;
}

static enum callbacker_io_type
_UT_callbacker_io_start__start_twice__helper_2(int fd,
                                               enum callbacker_io_type CALLBACKER_UNUSED type,
                                               void * CALLBACKER_NONNULL ud)
{
	assert(fd >= 0);
	assert(ud);

	char buf[100];
	int t;
	atomic_int * i = (atomic_int *)ud;

	t = read(fd, buf, sizeof(buf));
	if (t != 6)
	{
		printf("%s: Expected t to be 6. Was %d.\n", __func__, t);
		abort();
	}

	atomic_fetch_add(i, 100);

	return CALLBACKER_IO_TYPE_NONE;
}

static void
UT_callbacker_io_start__start_twice()
{
	size_t const buf_size = 6 * callbacker_data_size();
	char * buf, * tmpbuf;
	int pipefd[2];
	atomic_int c = 0;

	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));
	assert_false(fcntl(pipefd[0], F_SETFL, O_NONBLOCK));
	buf = test_malloc(buf_size);
	tmpbuf = test_malloc(100);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_int_equal(write(pipefd[1], "FOOBAR", 6), 6);
	assert_non_null(callbacker_io_start(pipefd[0],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_DEFAULT_PRIORITY,
	                                    _UT_callbacker_io_start__start_twice__helper_1,
	                                    &c));

	assert_non_null(callbacker_io_start(pipefd[0],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_DEFAULT_PRIORITY,
	                                    _UT_callbacker_io_start__start_twice__helper_2,
	                                    &c));
	assert_int_equal(write(pipefd[1], "FOOBAR", 6), 6);

	for (int i = 0 ; i < 100 && c != 101; i++)
		reasonable_sleep();

	assert_int_equal(c, 101);

	close(pipefd[1]);
	close(pipefd[0]);

	callbacker_uninitialize();

	test_free(buf);
	test_free(tmpbuf);
}

/***********************************************************************/

struct _UT_callbacker_io_start__chained__link
{
	int rw[2];
	char buffer[1000];
	int length;
	struct lock lock;
	void * read_handle;
	void * write_handle;
	unsigned int rb, wb;
};

static enum callbacker_io_type _UT_callbacker_io_start__chained__read(int, enum callbacker_io_type, void *);

static enum callbacker_io_type
_UT_callbacker_io_start__chained__write(int dst, enum callbacker_io_type type, void * user_data)
{
	assert(dst > 0);
	assert(user_data);

	struct _UT_callbacker_io_start__chained__link * link = (struct _UT_callbacker_io_start__chained__link *)user_data;
	int tmp;
	enum callbacker_io_type rv = CALLBACKER_IO_TYPE_KEEP;

	if (type & CALLBACKER_IO_TYPE_TRIGGER)
		return CALLBACKER_IO_TYPE_NONE;

	lock_acquire(&link->lock);

	tmp = write(dst, link->buffer, link->length);
	// printf("wrote to %d: %*s\n", dst, link->length, link->buffer);
	if (!(rand() % 10))
		reasonable_sleep(); // This reveals some underlying issues.
	assert_true(tmp >= 0);
	link->wb += tmp;

	for (int i = 0 ; i < link->length ; i++)
	{
		if (i < link->length - tmp)
			link->buffer[i] = link->buffer[i + tmp];
		else
			link->buffer[i] = 0;
	}

	link->length -= tmp;

	if (!link->length)
	{
		link->write_handle = NULL;
		rv = CALLBACKER_IO_TYPE_NONE;
	}

	if (!link->read_handle)
	{
		link->read_handle = callbacker_io_start(link->rw[0],
		                                        CALLBACKER_IO_TYPE_READ,
		                                        CALLBACKER_DEFAULT_PRIORITY,
		                                        _UT_callbacker_io_start__chained__read,
		                                        link);
		assert_non_null(link->read_handle);
	}

	lock_release(&link->lock);

	return rv;
}

static enum callbacker_io_type
_UT_callbacker_io_start__chained__read(int src, enum callbacker_io_type type, void * user_data)
{
	assert(src > 0);
	assert(user_data);

	struct _UT_callbacker_io_start__chained__link * link = (struct _UT_callbacker_io_start__chained__link *)user_data;
	int tmp;
	enum callbacker_io_type rv = CALLBACKER_IO_TYPE_KEEP;

	if (type & CALLBACKER_IO_TYPE_TRIGGER)
		return CALLBACKER_IO_TYPE_NONE;

	lock_acquire(&link->lock);

	tmp = read(src, link->buffer + link->length, sizeof(link->buffer) - link->length);
	// printf("read from %d: %*s\n", src, tmp, link->buffer + link->length);
	assert_true(tmp >= 0);
	link->rb += tmp;

	link->length += tmp;

	if (link->length == sizeof(link->buffer))
	{
		link->read_handle = NULL;
		rv = CALLBACKER_IO_TYPE_NONE;
	}

	if (!link->write_handle)
	{
		link->write_handle = callbacker_io_start(link->rw[1],
		                                         CALLBACKER_IO_TYPE_WRITE,
		                                         CALLBACKER_DEFAULT_PRIORITY,
		                                         _UT_callbacker_io_start__chained__write,
		                                         link);
		assert_non_null(link->write_handle);
	}

	lock_release(&link->lock);

	return rv;
}

static void
UT_callbacker_io_start__chained()
{
	enum
	{
		thread_count = 4,
		link_count = thread_count * 20,
		buf_size = 2 * (link_count + 1) * 0x48,
		rwbuf_size = 10000
	};
	char * buf = test_malloc(buf_size),
	     * rwbuf = test_malloc(rwbuf_size);
	assert_non_null(buf);
	assert_non_null(rwbuf);
	struct _UT_callbacker_io_start__chained__link * links
		= test_malloc(sizeof(struct _UT_callbacker_io_start__chained__link) * (link_count + 1));
	assert_non_null(links);
	int start, end;
	char * test_strings[] =
	{
		"testimies",
		"hulabalooba",
		" 1 ljksafdjlasdflkjsdaölijflaweifjoasiejfölaksfjölaisejfölaskdjföl",
		" 2 ljksafdjlasdflkjsdaölijflaweifjoasiejfölaksfjölaisejfölaskdjföl"
		" 3 ljksafdjlasdflkjsdaölijflaweifjoasiejfölaksfjölaisejfölaskdjföl"
		" 4 ljksafdjlasdflkjsdaölijflaweifjoasiejfölaksfjölaisejfölaskdjföl"
		" 5 ljksafdjlasdflkjsdaölijflaweifjoasiejfölaksfjölaisejfölaskdjföl"
		" 6 ljksafdjlasdflkjsdaölijflaweifjoasiejfölaksfjölaisejfölaskdjföl"
		" 7 ljksafdjlasdjlkjsdaölijflaweifjoasiejfölaksfjölaisejfölaskdjföl",
		A_LONG_ASS_STRING,
		"",
		"1",
		"over-and-out"
	};

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_count_set(thread_count));

	int holders[2] = {0};
	for (int i = 0; i < link_count + 1; i++)
	{
		memset(&links[i], 0, sizeof(links[i]));
		if (i < link_count)
			assert_false(pipe(links[i].rw));
		holders[1] = links[i].rw[0];
		links[i].rw[0] = holders[0];
		holders[0] = holders[1];
		memset(links[i].buffer, 0, sizeof(links[i].buffer));
		assert_false(lock_initialize(&links[i].lock));
		links[i].length = 0;
		links[i].write_handle = NULL;
		// printf("pipe %d: %d + %d\n", i, links[i].rw[0], links[i].rw[1]);
		if (i == 0 || i == link_count)
			links[0].read_handle = NULL;
		else
		{
			// printf("io_read for %d\n", links[i].rw[0]);
			links[i].read_handle = callbacker_io_start(links[i].rw[0],
			                                           CALLBACKER_IO_TYPE_READ,
			                                           CALLBACKER_DEFAULT_PRIORITY,
			                                           _UT_callbacker_io_start__chained__read,
			                                           &links[i]);
			assert_non_null(links[i].read_handle);
		}
	}

	start = links[0].rw[1];
	end = links[link_count].rw[0];
	// printf("start=%d, end=%d\n", start, end);

	for (unsigned int i = 0 ; i < sizeof(test_strings)/sizeof(char*) ; i++)
	{
		size_t l = strlen(test_strings[i]);
		assert_int_equal(write(start, test_strings[i], l), l);
	}

	for (unsigned int i = 0 ; i < sizeof(test_strings)/sizeof(char*) ; i++)
	{
		size_t l = strlen(test_strings[i]), ll = 0;
		while (ll < l)
		{
			int j = read(end, rwbuf + ll, l - ll);
			assert_true(j > 0);
			ll += j;
		}
		rwbuf[l] = '\0';
		assert_string_equal(rwbuf, test_strings[i]);
		// printf("in the end got: %s\n", rwbuf);
	}

	assert_false(fcntl(end, F_SETFL, O_NONBLOCK));
	assert_int_equal(read(end, rwbuf, rwbuf_size), -1);
	assert_int_equal(errno, EAGAIN);

	callbacker_uninitialize();

	for (int i = 0 ; i < link_count ; i++)
	{
		close(links[i].rw[0]);
		close(links[i].rw[1]);
		lock_uninitialize(&links[i].lock);
	}

	test_free(buf);
	test_free(rwbuf);
	test_free(links);
}

/***********************************************************************/

static enum callbacker_io_type
_UT_callbacker_io_start__HUP__helper(int fd, enum callbacker_io_type type, void * arg)
{
	assert(fd);

	struct holder * h = (struct holder *)arg;
	int err, tmp;
	enum callbacker_io_type rv = CALLBACKER_IO_TYPE_READ;

	lock_acquire(&h->lock);

	if (type & CALLBACKER_IO_TYPE_HANGUP)
	{
		atomic_fetch_add(&h->j, 1);
		rv = CALLBACKER_IO_TYPE_NONE;
	}
	else if (type & CALLBACKER_IO_TYPE_ERROR)
	{
		h->error++;
		rv = CALLBACKER_IO_TYPE_NONE;
	}
	else if (type & CALLBACKER_IO_TYPE_READ)
	{
		if ((err = read(fd, &tmp, sizeof(tmp))) != sizeof(tmp))
		{
			rv = CALLBACKER_IO_TYPE_NONE;
			h->error += 100;
		}
		atomic_fetch_add(&h->i, tmp);
	}
	else
	{
		rv = CALLBACKER_IO_TYPE_NONE;
		h->error += 10000;
	}

	condition_signal(&h->cond);
	lock_release(&h->lock);

	return rv;
}

static void
UT_callbacker_io_start__HANGUP()
{
	size_t const buf_size = 6 * callbacker_data_size();
	char * buf;
	int pipefd[2], tmp;
	struct holder h;

	holder_initialize(&h);
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));
	buf = test_malloc(buf_size);
	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(fcntl(pipefd[0], F_SETFL, O_NONBLOCK));

	assert_false(callbacker_threads_start(1));

	assert_non_null(callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_READ,
				CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_io_start__HUP__helper, &h));

	lock_acquire(&h.lock);

	tmp = 1;
	assert_int_equal(write(pipefd[1], &tmp, sizeof(tmp)), sizeof(tmp));
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 1);

	tmp = 2;
	assert_int_equal(write(pipefd[1], &tmp, sizeof(tmp)), sizeof(tmp));
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.i), 3);

	for (tmp = 3 ; tmp < 10 ; tmp++)
		assert_int_equal(write(pipefd[1], &tmp, sizeof(tmp)), sizeof(tmp));

	for (tmp = 3 ; tmp < 10 ; tmp++)
	{
		if (atomic_load(&h.i) == 45)
			break;
		assert_false(condition_timedwait(&h.cond, &half_sec));
	}
	assert_int_equal(atomic_load(&h.i), 45);
	assert_true(tmp <= 10);

	assert_int_equal(h.error, 0);
	assert_int_equal(atomic_load(&h.j), 0);
	assert_false(shutdown(pipefd[1], SHUT_RDWR));
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(atomic_load(&h.j), 1);
	assert_int_equal(h.error, 0);

	lock_release(&h.lock);

	assert_false(callbacker_threads_count_set(0));
	callbacker_uninitialize();
	holder_uninitialize(&h);

	close(pipefd[1]);
	close(pipefd[0]);

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_io_start__cleanup()
{
	size_t const buf_size = 6 * callbacker_data_size();
	char * buf;
	int pipefd[2];

	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));
	buf = test_malloc(buf_size);
	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(fcntl(pipefd[0], F_SETFL, O_NONBLOCK));

	assert_false(callbacker_threads_start(1));

	assert_non_null(callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_READ,
				CALLBACKER_DEFAULT_PRIORITY, dummy_io_handler, NULL));

	assert_non_null(callbacker_io_start(pipefd[1], CALLBACKER_IO_TYPE_WRITE,
				CALLBACKER_DEFAULT_PRIORITY, dummy_io_handler, NULL));

	callbacker_uninitialize();

	close(pipefd[0]);
	close(pipefd[1]);

	test_free(buf);
}

/***********************************************************************/

#define _UT_callbacker_io_start__reuse_fds__helper_X(IDX, COUNT) \
	static enum callbacker_io_type \
	_UT_callbacker_io_start__reuse_fds__helper_##IDX( \
	                int fd, \
	                enum callbacker_io_type type, \
	                void * arg) \
	{ \
		assert(fd > 0); \
		assert(type == CALLBACKER_IO_TYPE_READ); \
\
		struct holder * h = (struct holder *)arg; \
\
		lock_acquire(&h->lock); \
\
		while (h->i != IDX) \
			assert_false(condition_timedwait(&h->cond, &half_sec)); \
		h->i = (h->i + 1) % COUNT; \
\
		condition_broadcast(&h->cond); \
		lock_release(&h->lock); \
\
		return CALLBACKER_IO_TYPE_NONE; \
	}

_UT_callbacker_io_start__reuse_fds__helper_X(0, 3)
_UT_callbacker_io_start__reuse_fds__helper_X(1, 3)
_UT_callbacker_io_start__reuse_fds__helper_X(2, 3)

#undef _UT_callbacker_io_start__reuse_fds__helper_X

static void
UT_callbacker_io_start__reuse_fds()
{
	enum { rounds = 1000 };
	size_t const buf_size = 5 * callbacker_data_size();
	char * buf;
	int pipefd[3][2];
	static int const tmp = 1;
	struct holder h;

	holder_initialize(&h);
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd[0]));
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd[1]));
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd[2]));
	assert_false(fcntl(pipefd[0][0], F_SETFL, O_NONBLOCK));
	assert_false(fcntl(pipefd[1][0], F_SETFL, O_NONBLOCK));
	assert_false(fcntl(pipefd[2][0], F_SETFL, O_NONBLOCK));
	buf = test_malloc(buf_size);
	assert_false(callbacker_initialize(buf, buf_size));

	assert_false(callbacker_threads_start(1));

	assert_int_equal(write(pipefd[0][1], &tmp, sizeof(tmp)), sizeof(tmp));
	assert_int_equal(write(pipefd[1][1], &tmp, sizeof(tmp)), sizeof(tmp));
	assert_int_equal(write(pipefd[2][1], &tmp, sizeof(tmp)), sizeof(tmp));

	lock_acquire(&h.lock);

	for (int i = 0 ; i < rounds ; i++)
	{
		enum callbacker_io_type (*fn) (int, enum callbacker_io_type, void *);

		switch (i % 3)
		{
			case 0: fn = _UT_callbacker_io_start__reuse_fds__helper_0; break;
			case 1: fn = _UT_callbacker_io_start__reuse_fds__helper_1; break;
			case 2: fn = _UT_callbacker_io_start__reuse_fds__helper_2; break;
			default: abort();
		}

		assert_non_null(callbacker_io_start(pipefd[i % 2][0],
		                                    CALLBACKER_IO_TYPE_READ,
		                                    CALLBACKER_DEFAULT_PRIORITY,
		                                    fn,
		                                    &h));
		assert_false(condition_timedwait(&h.cond, &half_sec));
	}

	lock_release(&h.lock);

	callbacker_uninitialize();

	holder_uninitialize(&h);
	for (int i = 0 ; i < 2 ; i++)
		for (int j = 0 ; i < 2 ; i++)
		close(pipefd[i][j]);

	test_free(buf);
}

/***********************************************************************/

static enum callbacker_io_type
_UT_callbacker_io_start__close__helper(int fd, enum callbacker_io_type t, void * CALLBACKER_NONNULL p)
{
	assert_true(fd > 0);
	assert_true(t == CALLBACKER_IO_TYPE_READ);
	assert_non_null(p);

	struct holder * h = (struct holder *)p;

	lock_acquire(&h->lock);
	close(fd);
	condition_broadcast(&h->cond);
	lock_release(&h->lock);

	return CALLBACKER_IO_TYPE_NONE;
}

static void
UT_callbacker_io_start__close()
{
	size_t const buf_size = 2 * callbacker_data_size();
	char * buf;
	int pipefd[2];
	struct holder h;
	int tmp = 1;

	holder_initialize(&h);
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));
	assert_false(fcntl(pipefd[0], F_SETFL, O_NONBLOCK));
	buf = test_malloc(buf_size);
	assert_false(callbacker_initialize(buf, buf_size));

	assert_false(callbacker_threads_start(1));

	assert_int_equal(write(pipefd[1], &tmp, sizeof(tmp)), sizeof(tmp));

	lock_acquire(&h.lock);

	assert_non_null(callbacker_io_start(pipefd[0],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_DEFAULT_PRIORITY,
	                                    _UT_callbacker_io_start__close__helper,
	                                    &h));

	assert_false(condition_timedwait(&h.cond, &half_sec));

	lock_release(&h.lock);

	callbacker_uninitialize();

	holder_uninitialize(&h);

	close(pipefd[0]);
	close(pipefd[1]);

	test_free(buf);
}

/***********************************************************************/

static enum callbacker_io_type
_UT_callbacker_io_start__priorities__low(int fd, enum callbacker_io_type type, void * p)
{
	assert(fd > 0);
	assert(p);

	struct holder * h = (struct holder *)p;
	static atomic_uint t = 0;
	atomic_uint tt;

	if (type & CALLBACKER_IO_TYPE_READ)
	{
		if (read(fd, &tt, sizeof(t)) != sizeof(t))
			h->error++;
		else if (tt != t)
			h->error++;
		atomic_fetch_add(&t, 1);
	}

	assert_int_equal(1000, h->i);
	assert_int_equal(1000, h->j);

	if (atomic_fetch_add(&h->k, 1) == 999)
		return CALLBACKER_IO_TYPE_NONE;

	return CALLBACKER_IO_TYPE_KEEP;
}

static enum callbacker_io_type
_UT_callbacker_io_start__priorities__default(int fd, enum callbacker_io_type type, void * p)
{
	assert(fd > 0);
	assert(p);

	struct holder * h = (struct holder *)p;
	static atomic_uint t = 0;
	atomic_uint tt;

	if (type & CALLBACKER_IO_TYPE_READ)
	{
		if (read(fd, &tt, sizeof(t)) != sizeof(t))
			h->error++;
		else if (tt != t)
			h->error++;
		atomic_fetch_add(&t, 1);
	}

	assert_int_equal(0, h->k);
	assert_int_equal(1000, h->i);

	if (atomic_fetch_add(&h->j, 1) == 999)
		return CALLBACKER_IO_TYPE_NONE;

	return CALLBACKER_IO_TYPE_KEEP;
}

static enum callbacker_io_type
_UT_callbacker_io_start__priorities__high(int fd, enum callbacker_io_type type, void * CALLBACKER_NONNULL p)
{
	assert(fd > 0);
	assert(p);
	assert(type & CALLBACKER_IO_TYPE_READ);

	struct holder * h = (struct holder *)p;
	static atomic_uint t = 0;
	atomic_uint tt;

	if (type & CALLBACKER_IO_TYPE_READ)
	{
		if (read(fd, &tt, sizeof(t)) != sizeof(t))
			h->error++;
		else if (tt != t)
			h->error++;
		atomic_fetch_add(&t, 1);
	}

	assert_int_equal(0, h->j);
	assert_int_equal(0, h->k);

	if (atomic_fetch_add(&h->i, 1) == 999)
		return CALLBACKER_IO_TYPE_NONE;

	return CALLBACKER_IO_TYPE_KEEP;
}

static enum callbacker_io_type
_UT_callbacker_io_start__priorities__writer(int fd, enum callbacker_io_type type, void * CALLBACKER_UNUSED p)
{
	assert(fd > 0);
	assert(type & CALLBACKER_IO_TYPE_WRITE);

	static atomic_uint foo[3] = {0};
	unsigned long idx = (unsigned long)p;

	assert_true(write(fd, &foo[idx], sizeof(foo[idx])) == sizeof(foo[idx]));

	atomic_fetch_add(&foo[idx], 1);

	return CALLBACKER_IO_TYPE_KEEP;
}

static void
UT_callbacker_io_start__priorities()
{
	size_t const buf_size = 3002 * callbacker_data_size();
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	int socks[3][2];
	struct holder h;

	holder_initialize(&h);
	assert_false(callbacker_initialize(buf, buf_size));

	for (unsigned long i = 0 ; i < 3 ; i++)
	{
		assert_int_equal(0, socketpair(AF_UNIX, SOCK_STREAM, 0, socks[i]));
		assert_false(fcntl(socks[i][0], F_SETFL, O_NONBLOCK));
		assert_false(fcntl(socks[i][1], F_SETFL, O_NONBLOCK));

		assert_non_null(callbacker_io_start(socks[i][0],
		                                    CALLBACKER_IO_TYPE_WRITE,
		                                    CALLBACKER_HIGH_PRIORITY,
		                                    _UT_callbacker_io_start__priorities__writer,
		                                    (void*)i));
	}

	assert_false(callbacker_exec_try());

	assert_non_null(callbacker_io_start(socks[0][1],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_HIGH_PRIORITY,
	                                    _UT_callbacker_io_start__priorities__high,
	                                    &h));

	assert_non_null(callbacker_io_start(socks[1][1],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_LOW_PRIORITY,
	                                    _UT_callbacker_io_start__priorities__low,
	                                    &h));

	assert_non_null(callbacker_io_start(socks[2][1],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_DEFAULT_PRIORITY,
	                                    _UT_callbacker_io_start__priorities__default,
	                                    &h));

	callbacker_exec_try();

	assert_int_equal(1000, h.i);
	assert_int_equal(1000, h.j);
	assert_int_equal(1000, h.k);
	assert_int_equal(0, h.error);

	holder_uninitialize(&h);
	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

static void
_UT_callbacker_signal_start__handler(int signal, void * user_data)
{
	assert(signal);
	assert(user_data);

	struct holder * h = (struct holder *)user_data;

	lock_acquire(&h->lock);
	atomic_fetch_add(&h->signals[signal], 1);
	condition_signal(&h->cond);
	lock_release(&h->lock);
}

static void
UT_callbacker_signal_start()
{

	enum { buf_size = 400 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	atomic_int counts[SIGRTMAX];
	struct holder h;

	holder_initialize(&h);
	assert_false(callbacker_initialize(buf, buf_size));

	for (int i = 0 ; i < SIGRTMAX ; i++)
		atomic_init(&counts[i], 0);

	mask_signal(SIGUSR1);

	callbacker_signal_start(CALLBACKER_HIGH_PRIORITY,
			_UT_callbacker_signal_start__handler, &h, SIGUSR1, SIGUSR2, SIGCONT);

	callbacker_signal_start(CALLBACKER_DEFAULT_PRIORITY,
			_UT_callbacker_signal_start__handler, &h, SIGHUP);

	assert_false(callbacker_threads_start(1));

	mask_signal(SIGUSR2);
	mask_signal(SIGHUP);

	lock_acquire(&h.lock);

	for (int i = 0 ; i < 10 ; i++)
	{
		kill(getpid(), SIGUSR1);
		assert_false(condition_timedwait(&h.cond, &half_sec));
	}

	for (int i = 0 ; i < 10 ; i++)
	{
		kill(getpid(), SIGUSR2);
		assert_false(condition_timedwait(&h.cond, &half_sec));
	}

	for (int i = 0 ; i < 10 ; i++)
	{
		kill(getpid(), SIGUSR1);
		assert_false(condition_timedwait(&h.cond, &half_sec));
		kill(getpid(), SIGUSR2);
		assert_false(condition_timedwait(&h.cond, &half_sec));

		if (i % 2)
		{
			kill(getpid(), SIGHUP);
			assert_false(condition_timedwait(&h.cond, &half_sec));
		}
	}

	assert_int_equal(atomic_load(&h.signals[SIGUSR1]), 20);
	assert_int_equal(atomic_load(&h.signals[SIGUSR2]), 20);
	assert_int_equal(atomic_load(&h.signals[SIGHUP]), 5);
	assert_int_equal(atomic_load(&h.signals[SIGCONT]), 0);
	assert_int_equal(atomic_load(&h.signals[SIGTRAP]), 0);

	lock_release(&h.lock);
	callbacker_uninitialize();

	unmask_signal(SIGUSR1);
	unmask_signal(SIGUSR2);
	unmask_signal(SIGHUP);
	unmask_signal(SIGCONT);

	test_free(buf);
}

/***********************************************************************/

static void
_UT_callbacker_run__parent__stopper(void * p CALLBACKER_UNUSED)
{
	callbacker_threads_stop(1);
}

static void
_UT_callbacker_run__parent__starter(void * p CALLBACKER_UNUSED)
{
	if (callbacker_threads_start(1))
		abort();
	callbacker_threads_stop(2);
	if (callbacker_run(CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_run__parent__stopper, NULL))
		abort();
}

static void
UT_callbacker_run__parent()
{
	size_t const buf_size = 20 * callbacker_data_size();
	char * buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_run__parent__starter, NULL));
	callbacker_exec();
	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

struct _UT_callbacker_io_control__alternate_by_retval__meta
{
	int error;
	int reads;
	int writes;
};

static enum callbacker_io_type
_UT_callbacker_io_start__alternate_by_retval__helper(int fd, enum callbacker_io_type type, void * metap)
{
	assert(fd);
	assert(type & (CALLBACKER_IO_TYPE_READ | CALLBACKER_IO_TYPE_WRITE));
	assert(metap);

	struct _UT_callbacker_io_control__alternate_by_retval__meta * meta =
		(struct _UT_callbacker_io_control__alternate_by_retval__meta *)metap;
	static char buf;

	if (type & CALLBACKER_IO_TYPE_READ)
	{
		if (read(fd, &buf, 1) != 1)
			meta->error++;
		else
			meta->reads++;
		return CALLBACKER_IO_TYPE_WRITE;
	}

	if (type & CALLBACKER_IO_TYPE_WRITE && buf != 'z')
	{
		++buf;
		if (write(fd, &buf, 1) != 1)
			meta->error++;
		else
			meta->writes++;
		return CALLBACKER_IO_TYPE_READ;
	}

	callbacker_threads_stop(0);

	return CALLBACKER_IO_TYPE_NONE;
}

static void
UT_callbacker_io_start__alternate_by_retval()
{
	size_t const buf_size = 16 * callbacker_data_size();
	char * buf;
	int pipefd[2];
	struct _UT_callbacker_io_control__alternate_by_retval__meta meta = {0};

	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));

	buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(fcntl(pipefd[0], F_SETFL, O_NONBLOCK));

	assert_false(callbacker_threads_start(1));

	assert_non_null(callbacker_io_start(pipefd[1], CALLBACKER_IO_TYPE_READ,
				CALLBACKER_HIGH_PRIORITY, echo_server, NULL));
	assert_int_equal(write(pipefd[0], "a", 1), 1);
	assert_non_null(callbacker_io_start(pipefd[0], CALLBACKER_IO_TYPE_READ,
				CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_io_start__alternate_by_retval__helper, &meta));
	callbacker_exec();

	callbacker_uninitialize();

	close(pipefd[0]);
	close(pipefd[1]);

	assert_int_equal(meta.error, 0);
	assert_int_equal(meta.reads, 26);
	assert_int_equal(meta.writes, 25);

	test_free(buf);
}

/***********************************************************************/

static enum callbacker_io_type
_UT_callbacker_io_trigger__helper(int fd, enum callbacker_io_type t, void * CALLBACKER_NONNULL p)
{
	assert_true(fd > 0);
	assert_non_null(p);

	struct holder * h = (struct holder *)p;
	char buf[100];

	lock_acquire(&h->lock);

	if (t & CALLBACKER_IO_TYPE_TRIGGER)
	{
		if (h->k == fd)
			atomic_fetch_add(&h->i, (t & CALLBACKER_IO_TYPE_READ) ? 100 : 1);
		else
			atomic_fetch_add(&h->j, (t & CALLBACKER_IO_TYPE_READ) ? 100 : 1);
	}
	else if (t & CALLBACKER_IO_TYPE_READ)
	{
		if (h->k == fd)
			atomic_fetch_add(&h->i, 50);
		else
			atomic_fetch_add(&h->j, 50);
	}

	if (t & CALLBACKER_IO_TYPE_READ)
		read(fd, buf, sizeof(buf));

	condition_broadcast(&h->cond);
	lock_release(&h->lock);

	return CALLBACKER_IO_TYPE_KEEP;
}

static void
UT_callbacker_io_trigger()
{
	size_t const buf_size = 4 * callbacker_data_size();
	char * buf;
	int pipefd[2];
	int tmp = 1;
	struct holder h;
	void * f, * s;

	holder_initialize(&h);
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));
	assert_false(fcntl(pipefd[0], F_SETFL, O_NONBLOCK));
	h.k = pipefd[0];
	buf = test_malloc(buf_size);
	assert_false(callbacker_initialize(buf, buf_size));

	assert_false(callbacker_threads_start(1));

	assert_non_null((f = callbacker_io_start(pipefd[0],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_DEFAULT_PRIORITY,
	                                    _UT_callbacker_io_trigger__helper,
	                                    &h)));

	assert_non_null((s = callbacker_io_start(pipefd[1],
	                                    CALLBACKER_IO_TYPE_READ,
	                                    CALLBACKER_DEFAULT_PRIORITY,
	                                    _UT_callbacker_io_trigger__helper,
	                                    &h)));

	lock_acquire(&h.lock);

	assert_true(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(h.i, 0);
	assert_int_equal(h.j, 0);

	callbacker_io_trigger(f);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(h.i, 1);
	assert_int_equal(h.j, 0);

	callbacker_io_trigger(f);
	callbacker_io_trigger(s);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_false(condition_timedwait(&h.cond, &half_sec) && false);
	assert_int_equal(h.i, 2);
	assert_int_equal(h.j, 1);

	for (int i = 0; i < 10 ; i++)
		callbacker_io_trigger(s);
	while (!condition_timedwait(&h.cond, &half_sec));
	while (!condition_timedwait(&h.cond, &half_sec));

	assert_int_equal(h.i, 2);
	assert_true(h.j >= 2);

	h.i = 0;
	h.j = 0;
	assert_int_equal(write(pipefd[1], &tmp, sizeof(tmp)), sizeof(tmp));
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(h.i, 50);
	assert_int_equal(h.j, 0);

	h.i = 0;
	h.j = 0;
	callbacker_io_trigger(f);
	callbacker_io_trigger(s);
	assert_int_equal(write(pipefd[1], &tmp, sizeof(tmp)), sizeof(tmp));
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_false(condition_timedwait(&h.cond, &half_sec) && false);
	assert_false(condition_timedwait(&h.cond, &half_sec) && false);
	assert_true(h.i == 51 || h.i == 100);
	assert_int_equal(h.j, 1);

	lock_release(&h.lock);

	callbacker_uninitialize();
	holder_uninitialize(&h);

	close(pipefd[0]);
	close(pipefd[1]);

	test_free(buf);
}

/***********************************************************************/

static enum callbacker_timer_type
_UT_callbacker_timer_trigger__helper(callbacker_timer_id_t id, enum callbacker_timer_type t, void * CALLBACKER_NONNULL p)
{
	assert(id);
	assert_non_null(p);

	struct holder * h = (struct holder *)p;

	if (t & CALLBACKER_TIMER_TYPE_TRIGGER)
		atomic_fetch_add(&h->i, 1);

	if (t & CALLBACKER_TIMER_TYPE_EXPIRATION)
		atomic_fetch_add(&h->j, 1);

	return CALLBACKER_TIMER_TYPE_EXPIRATION;
}

static void
UT_callbacker_timer_trigger()
{
	enum { parcount = 4, outer = 5, inner = 10 };
	size_t const buf_size = parcount * callbacker_data_size();
	char * buf;
	struct holder h;
	callbacker_timer_id_t ids[parcount];
	struct timespec delay = { 0, 1000*1000*100 };

	holder_initialize(&h);
	buf = test_malloc(buf_size);
	assert_false(callbacker_initialize(buf, buf_size));

	assert_false(callbacker_threads_start(2));

	for (int i = 0 ; i < parcount ; i++)
	{
		ids[i] = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
	                                            &delay,
	                                            &delay,
	                                            _UT_callbacker_timer_trigger__helper,
	                                            &h);
		assert_non_null(ids[i]);
	}

	delay.tv_nsec += 30*1000*1000;

	for (int k = outer ; k ; k--)
	{
		assert_false(nanosleep(&delay, NULL));

		for (int j = inner ; j ; --j)
		{
			for (int i = 0 ; i < parcount ; ++i)
				callbacker_timer_trigger(ids[i]);

			assert_false(nanosleep(&delay, NULL));
		}
	}

	callbacker_threads_stop(0);

	printf("h.i=%d, %d\n", h.i, outer + outer * inner * parcount);
	assert_true(h.j >= outer + outer * inner * parcount);
	assert_true(h.j < 2 * (outer + outer * inner * parcount));
	assert_int_equal(h.i, outer * inner * parcount);

	callbacker_uninitialize();
	holder_uninitialize(&h);

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_io_trigger__NULL()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	expect_assert_failure(callbacker_io_trigger(NULL));
	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_timer_trigger__NULL()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	expect_assert_failure(callbacker_timer_trigger(NULL));
	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_io_trigger__uninitialized()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure(callbacker_io_trigger((void*)buf));

	test_free(buf);
}

/***********************************************************************/

static enum callbacker_io_type
_UT_callbacker_io_trigger__trigger_running__helper(int fd, enum callbacker_io_type type, void * ud)
{
	assert(fd >= 0);
	assert(type);

	char buf[10];
	int rb = 0;
	struct holder * h = (struct holder *)ud;

	lock_acquire(&h->lock);

	if (type & CALLBACKER_IO_TYPE_TRIGGER)
	{
		assert(h->i == 2);
		h->i = 3;
		condition_signal(&h->cond);
		lock_release(&h->lock);
		return CALLBACKER_IO_TYPE_NONE;
	}

	assert(h->i == 0);
	h->i = 1;
	condition_signal(&h->cond);
	lock_release(&h->lock);

	printf("reading\n");
	while (rb < 10)
		rb += read(fd, buf, 10 - rb);
	printf("done reading\n");

	lock_acquire(&h->lock);
	h->i = 2;
	condition_signal(&h->cond);
	lock_release(&h->lock);

	printf("Keeping\n");
	return CALLBACKER_IO_TYPE_KEEP;
}

static void
UT_callbacker_io_trigger__trigger_running()
{
	size_t buf_size = callbacker_data_size();
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	callbacker_io_id_t id;
	int pipefd[2];
	struct holder h;

	holder_initialize(&h);
	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(2));
	assert_false(socketpair(AF_UNIX, SOCK_STREAM, 0, pipefd));
	id = callbacker_io_start(pipefd[0],
	                         CALLBACKER_IO_TYPE_READ,
	                         CALLBACKER_DEFAULT_PRIORITY,
	                         _UT_callbacker_io_trigger__trigger_running__helper,
	                         &h);
	assert_true(id);

	lock_acquire(&h.lock);

	assert_int_equal(h.i, 0);
	assert_int_equal(write(pipefd[1], "12345", 5), 5);

	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_equal(h.i, 1);

	callbacker_io_trigger(id);

	assert_int_equal(h.i, 1);

	assert_int_equal(write(pipefd[1], "67890", 5), 5);

	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_int_not_equal(h.i, 1);

	if (h.i != 3)
		assert_false(condition_timedwait(&h.cond, &half_sec));

	assert_int_equal(h.i, 3);

	lock_release(&h.lock);

	callbacker_uninitialize();
	holder_uninitialize(&h);
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_timer_trigger__uninitialized()
{
	enum { buf_size = 200 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure(callbacker_timer_trigger((void*)buf));

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_signal_start__uninitialized()
{
	enum { buf_size = 100 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure(
			callbacker_signal_start(CALLBACKER_DEFAULT_PRIORITY,
			dummy_signal_handler, NULL, SIGUSR1));

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_signal_start__NULL()
{
	enum { buf_size = 100 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));

	expect_assert_failure(callbacker_signal_start(0, dummy_signal_handler, SNEAKY_NULL, 0));
	expect_assert_failure(callbacker_signal_start(0, SNEAKY_NULL_SIGNAL, SNEAKY_NULL, SIGUSR1));

	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_signal_start__memory()
{
	int buf_size = 3 * callbacker_data_size();
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));

	callbacker_signal_start(CALLBACKER_DEFAULT_PRIORITY, dummy_signal_handler, NULL, SIGUSR1, SIGUSR2);
	callbacker_signal_start(CALLBACKER_DEFAULT_PRIORITY, dummy_signal_handler, NULL, SIGINT, SIGUSR2);
	callbacker_signal_start(CALLBACKER_DEFAULT_PRIORITY, dummy_signal_handler, NULL, SIGHUP, SIGUSR2, SIGINT);
	expect_assert_failure(callbacker_signal_start(CALLBACKER_DEFAULT_PRIORITY, dummy_signal_handler, NULL, SIGCONT));

	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_signal_start__already_running()
{
	enum { buf_size = 100 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));

	expect_assert_failure(
			callbacker_signal_start(CALLBACKER_DEFAULT_PRIORITY,
			dummy_signal_handler, NULL, SIGUSR1));

	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

/**
 * Implement this here to make unit test coverage test not complain about
 * this missing.
 */
CALLBACKER_UNUSED
static void
UT__callbacker_signal_start()
{
}

/***********************************************************************/

static void
_UT_callbacker_exec__deadend(void * p)
{
	assert(p);

	atomic_int * counter = (atomic_int *)p;

	atomic_fetch_add(counter, 1);
}

static void
_UT_callbacker_exec__starter(void * p)
{
	assert(p);

	atomic_int * counter = (atomic_int *)p;

	atomic_fetch_add(counter, 10);
	assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, _UT_callbacker_exec__deadend, p));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_exec__deadend, p));

}

static void
_UT_callbacker_exec__stopper(void * p)
{
	assert(p);

	atomic_int * counter = (atomic_int *)p;

	atomic_fetch_add(counter, 100);
	assert_false(callbacker_threads_count_set(0));
}

static void
UT_callbacker_exec()
{
	enum { buf_size = 500 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	atomic_int c;

	atomic_init(&c, 0);
	assert_false(callbacker_initialize(buf, buf_size));

	assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, _UT_callbacker_exec__starter, &c));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_exec__deadend, &c));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, _UT_callbacker_exec__deadend, &c));
	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, _UT_callbacker_exec__stopper, &c));

	callbacker_exec();
	callbacker_uninitialize();

	assert_int_equal(atomic_load(&c), 114);
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_exec__uninitialized()
{
	enum { buf_size = 400 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure(callbacker_exec());

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_exec_try()
{
	enum { buf_size = 600 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));

	assert_true(callbacker_exec_try());

	assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, dummy_sleeper, NULL));

	assert_false(callbacker_exec_try());

	assert_true(callbacker_exec_try());

	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_exec_try());
	assert_true(callbacker_exec_try());

	assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_exec_try());
	assert_true(callbacker_exec_try());

	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_exec_try());
	assert_true(callbacker_exec_try());

	assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, dummy_sleeper, NULL));
	assert_false(callbacker_exec_try());
	assert_true(callbacker_exec_try());

	callbacker_uninitialize();

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_exec_try__uninitialized()
{
	enum { buf_size = 400 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);

	assert_false(callbacker_initialize(buf, buf_size));
	callbacker_uninitialize();

	expect_assert_failure(callbacker_exec_try());

	test_free(buf);
}

/***********************************************************************/

static void
_UT_callbacker_exec_try__multiple__helper(void * p CALLBACKER_UNUSED)
{
	reasonable_sleep();
	callbacker_exec_try();
}

static void
_UT_callbacker_exec_try__multiple(int thread_count, int real_threads)
{
	char * buf = test_malloc(700 * thread_count);
	assert_non_null(buf);

	printf("Running with %d \"threads\" and %d real threads.\n", thread_count, real_threads);

	assert_false(callbacker_initialize(buf, 500 * thread_count));

	for (int i = 0 ; i < thread_count ; i++)
		assert_false(callbacker_run(CALLBACKER_HIGH_PRIORITY, _UT_callbacker_exec_try__multiple__helper, NULL));

	for (int i = 0 ; i < 2 * thread_count ; i++)
		assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper__long, NULL));

	assert_false(callbacker_threads_count_set(real_threads - 1));
	callbacker_exec_try();
	callbacker_uninitialize();

	test_free(buf);
}

static void
UT_callbacker_exec_try__multiple()
{
	for (int i = 1 ; i <= 10 ; i++)
		_UT_callbacker_exec_try__multiple(10, i);
}

/***********************************************************************/

static enum callbacker_timer_type
_UT_callbacker_timer_start__helper(callbacker_timer_id_t id,
                                   enum callbacker_timer_type type,
                                   void * data)
{
	assert(id);
	assert(type);
	assert(data);

	struct holder * h = (struct holder *)data;

	lock_acquire(&h->lock);
	condition_signal(&h->cond);
	h->i++;
	lock_release(&h->lock);

	return CALLBACKER_TIMER_TYPE_NONE;
}

static void
UT_callbacker_timer_start()
{
	enum { buf_size = 2000, node_count = 100, loops = 10 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	assert_false(callbacker_initialize(buf, buf_size));
	struct timespec d = { 0, 100*1000*1000 };
	void * ids[10];
	atomic_long counts[node_count];
	struct holder h;

	assert_false(callbacker_threads_start(2));
	holder_initialize(&h);

	lock_acquire(&h.lock);

	for (long i = 0 ; i < loops ; i++)
	{
		atomic_init(&counts[i], 0);

		ids[i] = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
				&d, &d, _UT_callbacker_timer_start__helper, &h);
		assert_non_null(ids[i]);
	}

	assert_false(condition_timedwait(&h.cond, &half_sec));
	for (int i = 0 ; i < loops - 1 ; i++)
		assert_false(condition_timedwait(&h.cond, &half_sec) && false);

	lock_release(&h.lock);

	reasonable_sleep();
	reasonable_sleep();
	reasonable_sleep();

	assert_int_equal(h.i, loops);

	callbacker_uninitialize();
	holder_uninitialize(&h);
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_timer_start__NULL()
{
	enum { buf_size = 1000 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	assert_false(callbacker_initialize(buf, buf_size));
	struct timespec d = { 1, 0 };
	void * dummy CALLBACKER_UNUSED;

	assert_false(callbacker_threads_start(1));

	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
					SNEAKY_NULL, SNEAKY_NULL, SNEAKY_NULL_TIMER, (void*)0x1)));

	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
					SNEAKY_NULL, SNEAKY_NULL, dummy_timer_handler, (void*)0x1)));
	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
					SNEAKY_NULL, &d, dummy_timer_handler, (void*)0x1)));
	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
				&d, SNEAKY_NULL, dummy_timer_handler, (void*)0x1)));

	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
				&d, &d, SNEAKY_NULL_TIMER, (void*)0x1)));

	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_timer_start__uninitialized()
{
	enum { buf_size = 1000 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	struct timespec d = { 1, 0 };
	void * dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));
	callbacker_uninitialize();

	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
	                                                      &d,
	                                                      &d,
	                                                      dummy_timer_handler,
	                                                      NULL)));

	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_timer_start__invalid()
{
	enum { buf_size = 1000 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	assert_false(callbacker_initialize(buf, buf_size));
	struct timespec d = { 0, 0 }, dd = { 1, 0 };
	void * dummy CALLBACKER_UNUSED;

	assert_false(callbacker_threads_start(1));

	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
					&d, &d, dummy_timer_handler, (void*)0x1)));
	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
					&d, &dd, dummy_timer_handler, (void*)0x1)));
	expect_assert_failure((dummy = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
					&dd, &d, dummy_timer_handler, (void*)0x1)));

	expect_assert_failure((dummy = callbacker_timer_start(100,
					&dd, &dd, dummy_timer_handler, (void*)0x1)));
	expect_assert_failure((dummy = callbacker_timer_start(-100,
					&dd, &dd, dummy_timer_handler, (void*)0x1)));

	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

static enum callbacker_timer_type
_UT_callbacker_timer_start__memory__helper(callbacker_timer_id_t id,
                                           enum callbacker_timer_type type,
                                           void * ud)
{
	assert(id);
	assert(type == CALLBACKER_TIMER_TYPE_TRIGGER);
	assert(ud);

	struct holder * h = (struct holder *)ud;

	lock_acquire(&h->lock);
	condition_signal(&h->cond);
	lock_release(&h->lock);

	return CALLBACKER_TIMER_TYPE_NONE;
}

static void
UT_callbacker_timer_start__memory()
{
	enum { ok_count = 3 };
	int buf_size = ok_count * callbacker_data_size();
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	struct timespec d = { 5, 0 };
	callbacker_timer_id_t subs[ok_count + 1];
	struct holder h;

	holder_initialize(&h);
	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(1));

	for (int i = 0 ; i < ok_count ; i++)
	{
		subs[i] = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
		                                 &d,
		                                 &d,
		                                 _UT_callbacker_timer_start__memory__helper,
		                                 &h);
		assert_non_null(subs[i]);
	}

	for (int i = 0 ; i < 3 ; i++)
	{
		assert_null(callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
		                                   &d,
		                                   &d,
		                                   _UT_callbacker_timer_start__memory__helper,
		                                   &h));
		assert_int_equal(errno, ENOMEM);
		errno = 0;
	}

	lock_acquire(&h.lock);

	for (int i = 0 ; i < ok_count ; i++)
	{
		callbacker_timer_trigger(subs[i]);
		assert_false(condition_timedwait(&h.cond, &half_sec));
		reasonable_sleep();

		subs[i] = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
		                                 &d,
		                                 &d,
		                                 _UT_callbacker_timer_start__memory__helper,
		                                 &h);
		assert_non_null(subs[i]);

		for (int i = 0 ; i < 3 ; i++)
		{
			assert_null(callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
			                                   &d,
			                                   &d,
			                                   _UT_callbacker_timer_start__memory__helper,
			                                   &h));
			assert_int_equal(errno, ENOMEM);
			errno = 0;
		}
	}


	for (int i = 0 ; i < ok_count ; i++)
		callbacker_timer_trigger(subs[i]);
	assert_false(condition_timedwait(&h.cond, &half_sec));
	assert_false(condition_timedwait(&h.cond, &half_sec) && false);
	assert_false(condition_timedwait(&h.cond, &half_sec) && false);
	reasonable_sleep();

	lock_release(&h.lock);

	for (int i = 0 ; i < ok_count ; i++)
	{
		subs[i] = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
		                                 &d,
		                                 &d,
		                                 _UT_callbacker_timer_start__memory__helper,
		                                 &h);
		assert_non_null(subs[i]);
	}

	for (int i = 0 ; i < 3 ; i++)
	{
		assert_null(callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
		                                   &d,
		                                   &d,
		                                   _UT_callbacker_timer_start__memory__helper,
		                                   &h));
		assert_int_equal(errno, ENOMEM);
		errno = 0;
	}

	callbacker_uninitialize();
	holder_uninitialize(&h);
	test_free(buf);
}

/***********************************************************************/


static void
UT_callbacker_timer_trigger__invalid()
{
	enum { buf_size = 1000 };
	char * buf = test_malloc(buf_size);

	assert_false(callbacker_initialize(buf, buf_size));

	expect_assert_failure(callbacker_timer_trigger(buf));

	callbacker_uninitialize();
	test_free(buf);
}

/***********************************************************************/

static enum callbacker_timer_type
_UT_callbacker_timer_trigger__running__helper(callbacker_timer_id_t id,
                                              enum callbacker_timer_type type,
                                              void * arg)
{
	assert(arg);
	assert(id);
	assert(type);

	struct holder * t = (struct holder *)arg;

	if (type & CALLBACKER_TIMER_TYPE_TRIGGER)
		return CALLBACKER_TIMER_TYPE_NONE;

	lock_acquire(&t->lock);
	t->i++;
	condition_notify_all(&t->cond);
	lock_release(&t->lock);

	reasonable_sleep();

	lock_acquire(&t->lock);
	t->i++;
	condition_notify_all(&t->cond);
	lock_release(&t->lock);

	return CALLBACKER_TIMER_TYPE_EXPIRATION;
}

static void
UT_callbacker_timer_trigger__stop_running()
{
	size_t const buf_size = 10 * callbacker_data_size();
	char * buf;
	struct callbacker_data * sub1, * sub2;
	struct timespec init_delay = { 0, 1000*1000 }, repeat_delay = { 5, 0 };
	struct holder h1, h2;

	buf = test_malloc(buf_size);
	holder_initialize(&h1);
	holder_initialize(&h2);
	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(2));

	lock_acquire(&h1.lock);
	lock_acquire(&h2.lock);

	sub1 = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
	                              &init_delay,
	                              &repeat_delay,
	                              _UT_callbacker_timer_trigger__running__helper,
	                              &h1);
	assert_non_null(sub1);

	sub2 = callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
	                              &init_delay,
	                              &repeat_delay,
	                              _UT_callbacker_timer_trigger__running__helper,
	                              &h2);
	assert_non_null(sub2);

	assert_false(condition_timedwait(&h1.cond, &repeat_delay));
	assert_true(h1.i == 1);

	if (h2.i != 1)
		assert_false(condition_timedwait(&h2.cond, &repeat_delay));

	callbacker_timer_trigger(sub1);
	assert_int_equal(1, h1.i);

	callbacker_timer_trigger(sub2);
	assert_int_equal(1, h2.i);

	assert_false(condition_timedwait(&h1.cond, &repeat_delay));
	assert_true(h1.i == 2);

	if (h2.i != 2)
		assert_false(condition_timedwait(&h2.cond, &repeat_delay));

	assert_int_equal(2, h1.i);
	assert_int_equal(2, h2.i);

	lock_release(&h1.lock);
	lock_release(&h2.lock);

	reasonable_sleep(); // Give time for stuff to break.

	assert_int_equal(2, h1.i);
	assert_int_equal(2, h2.i);

	callbacker_uninitialize();
	test_free(buf);

	assert_int_equal(2, h1.i);
	assert_int_equal(2, h2.i);

	holder_uninitialize(&h1);
	holder_uninitialize(&h2);
}

/***********************************************************************/

#define _UT_callbacker_timer_start__return_true__counter_count 100
static atomic_int _UT_callbacker_timer_start__return_true__counters[_UT_callbacker_timer_start__return_true__counter_count];
static atomic_int _UT_callbacker_timer_start__return_true__loops[_UT_callbacker_timer_start__return_true__counter_count];

static enum callbacker_timer_type
_UT_callbacker_timer_start__return_true__helper(callbacker_timer_id_t id,
                                                enum callbacker_timer_type type,
                                                void * data)
{
	assert(id);
	assert(type == CALLBACKER_TIMER_TYPE_EXPIRATION);

	long idx = (long)data;

	if (atomic_fetch_add(&_UT_callbacker_timer_start__return_true__counters[idx], 1)
			>= _UT_callbacker_timer_start__return_true__loops[idx])
		return CALLBACKER_TIMER_TYPE_NONE;

	return CALLBACKER_TIMER_TYPE_EXPIRATION;
}

static void
UT_callbacker_timer_start__return_true()
{
	enum { buf_size = 2000 };
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	assert_false(callbacker_initialize(buf, buf_size));
	struct timespec d = { 0 };

	for (int i = 0 ; i < _UT_callbacker_timer_start__return_true__counter_count ; i++)
		atomic_init(&_UT_callbacker_timer_start__return_true__counters[i], 0);

	assert_false(callbacker_threads_start(2));

	for (long i = 0 ; i < 10 ; i++)
	{
		atomic_init(&_UT_callbacker_timer_start__return_true__loops[i], 0);

		d.tv_nsec = 100 * 1000 * 1000;
		assert_non_null(callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
				&d, &d, _UT_callbacker_timer_start__return_true__helper, (void*)i));

		d.tv_nsec = 60 * 1000 * 1000;
		assert_false(nanosleep(&d, NULL));
		assert_int_equal(atomic_load(&_UT_callbacker_timer_start__return_true__counters[i]), 0);

		assert_false(nanosleep(&d, NULL));
		assert_int_equal(atomic_load(&_UT_callbacker_timer_start__return_true__counters[i]), 1);
	}

	memset(_UT_callbacker_timer_start__return_true__counters, 0, sizeof(_UT_callbacker_timer_start__return_true__counters));
	for (long i = 0 ; i < 10 ; i++)
	{
		d.tv_nsec = (10 - i) * 1000 * 1000;
		atomic_init(&_UT_callbacker_timer_start__return_true__loops[i], i * 0x10);
		assert_non_null(callbacker_timer_start(CALLBACKER_DEFAULT_PRIORITY,
				&d, &d, _UT_callbacker_timer_start__return_true__helper, (void*)i));
	}

	if (VALGRIND)
		sleep(5);
	else
		sleep(1);

	for (int i = 0 ; i < 10 ; i++)
		assert_int_equal(_UT_callbacker_timer_start__return_true__counters[i], i*0x10 + 1);

	callbacker_uninitialize();
	test_free(buf);
}

#undef _UT_callbacker_timer_start__return_true__counter_count

/***********************************************************************/

static void
UT_callbacker_data_size()
{
	/** Just to make sure we don't alter this by accident. */
	assert_int_equal(callbacker_data_size(), 0x40);
}

/***********************************************************************/

static void *
_UT_callbacker_run_resolvable__helper(void * CALLBACKER_NULLABLE arg)
{
	struct timespec delay = {0, (rand()%100) * 1000 * 1000};

	if (nanosleep(&delay, NULL))
		abort();

	return arg;
}

static void
UT_callbacker_run_resolvable()
{
	const unsigned retvals = 10;
	const unsigned buf_size = retvals * callbacker_data_size();
	struct callbacker_retval * rvs
	    = (struct callbacker_retval *)test_malloc(sizeof(struct callbacker_retval) * retvals);
	assert_non_null(rvs);
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	for (unsigned long i = 0 ; i < retvals ; i++)
		assert_false(callbacker_run_resolvable(&rvs[i],
		                                      CALLBACKER_DEFAULT_PRIORITY,
		                                      _UT_callbacker_run_resolvable__helper,
		                                      (void *)i));

	for (unsigned long i = 0 ; i < retvals ; i++)
		assert_int_equal(i, (unsigned long)callbacker_resolve(&rvs[i]));

	callbacker_threads_stop(0);
	callbacker_uninitialize();

	test_free(rvs);
	test_free(unit_buffer);
}

/***********************************************************************/

static void
UT_callbacker_run_resolvable__NULL()
{
	struct callbacker_retval * rv = test_malloc(sizeof(struct callbacker_retval));
	char * buf = test_malloc(123);
	assert_non_null(buf);
	bool CALLBACKER_UNUSED tmp;

	assert_false(callbacker_initialize(buf, 123));
	assert_false(callbacker_threads_start(1));

	expect_assert_failure((tmp = callbacker_run_resolvable(SNEAKY_NULL,
	                                               CALLBACKER_DEFAULT_PRIORITY,
	                                               SNEAKY_NULL_DIRECT_RESOLVABLE,
	                                               (void *)0x1337)));

	expect_assert_failure((tmp = callbacker_run_resolvable(rv,
	                                               CALLBACKER_DEFAULT_PRIORITY,
	                                               SNEAKY_NULL_DIRECT_RESOLVABLE,
	                                               (void *)0x1337)));

	expect_assert_failure((tmp = callbacker_run_resolvable(SNEAKY_NULL,
	                                               CALLBACKER_DEFAULT_PRIORITY,
	                                               resolvable_dummy_sleeper,
	                                               (void *)0x1337)));

	assert_false(callbacker_run_resolvable(rv,
	                                      CALLBACKER_DEFAULT_PRIORITY,
	                                      resolvable_dummy_sleeper,
	                                      (void *)0x1337));

	assert_ptr_equal(callbacker_resolve(rv), (void*)(2 * 0x1337));

	callbacker_uninitialize();
	test_free(rv);
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_run_resolvable__unintialized()
{
	struct callbacker_retval * rv = test_malloc(sizeof(struct callbacker_retval));
	char * buf = test_malloc(123);
	assert_non_null(buf);
	bool CALLBACKER_UNUSED tmp;

	assert_false(callbacker_initialize(buf, 123));
	assert_false(callbacker_threads_start(1));
	callbacker_uninitialize();

	expect_assert_failure((tmp = callbacker_run_resolvable(rv,
	                                               CALLBACKER_DEFAULT_PRIORITY,
	                                               SNEAKY_NULL_DIRECT_RESOLVABLE,
	                                               (void *)0x1337)));

	test_free(rv);
	test_free(buf);
}

/***********************************************************************/

static void
UT_callbacker_run_resolvable__memory()
{
	enum { ok_count = 5, iter_count = 10, fail_count = 3 };
	int buf_size = ok_count * callbacker_data_size();
	char * buf = test_malloc(buf_size);
	assert_non_null(buf);
	struct callbacker_retval * rvs = test_malloc((ok_count + 1) * sizeof(struct callbacker_retval));
	assert_non_null(rvs);
	bool dummy CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(buf, buf_size));
	assert_false(callbacker_threads_start(10));
	errno = 0;

	for (int j = 0 ; j < iter_count ; j++)
	{
		for (int i = 0 ; i < ok_count ; i++)
			assert_false(callbacker_run_resolvable(&rvs[i],
			                                       CALLBACKER_DEFAULT_PRIORITY,
			                                       resolvable_dummy_sleeper,
			                                       (void *)(1ul * i)));

		for (int i = 0 ; i < fail_count ; i++)
		{
			assert_true(callbacker_run_resolvable(&rvs[ok_count],
			                                      CALLBACKER_DEFAULT_PRIORITY,
			                                      resolvable_dummy_sleeper,
			                                      NULL));
			assert_int_equal(errno, ENOMEM);
			errno = 0;
		}

		assert_true(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));
		assert_int_equal(errno, ENOMEM);
		errno = 0;

		for (int i = 0 ; i < 5 ; i++)
			reasonable_sleep();

		for (int i = 0 ; i < ok_count ; i++)
			assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, dummy_sleeper, NULL));

		for (int i = 0 ; i < 5 ; i++)
			assert_ptr_equal(callbacker_resolve(&rvs[i]), (void*)(2ul * i));

		for (int i = 0 ; i < 5 ; i++)
			reasonable_sleep();
	}

	callbacker_uninitialize();

	test_free(buf);
	test_free(rvs);
}

/***********************************************************************/

struct _UT_callbacker_run_resolvable_X__struct
{
	int a, b, c;
	char d;
};

static struct _UT_callbacker_run_resolvable_X__struct *
_UT_callbacker_run_resolvable__helper__my_struct(
		struct _UT_callbacker_run_resolvable_X__struct * CALLBACKER_NULLABLE sp)
{
	assert(sp);

	reasonable_sleep();

	return sp + 2;
}

static short *
_UT_callbacker_run_resolvable__helper__my_short(short * CALLBACKER_NULLABLE sp CALLBACKER_UNUSED)
{
	assert(sp);

	reasonable_sleep();

	return sp + 1;
}

CALLBACKER_RETVAL(struct _UT_callbacker_run_resolvable_X__struct, my_struct)
CALLBACKER_RETVAL(short, my_short)

static void
UT_callbacker_run_resolvable_X()
{
	enum { buf_size = 200 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval_my_struct rv1;
	struct callbacker_retval_my_short rv2;
	struct _UT_callbacker_run_resolvable_X__struct arg1;
	short arg2;
	struct _UT_callbacker_run_resolvable_X__struct * rrv1;
	short * rrv2;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_false(callbacker_run_resolvable_my_struct(&rv1,
	                                                 CALLBACKER_DEFAULT_PRIORITY,
	                                                 _UT_callbacker_run_resolvable__helper__my_struct,
	                                                 &arg1));

	assert_true(callbacker_resolve_try_my_struct(&rv1, &rrv1));

	assert_false(callbacker_run_resolvable_my_short(&rv2,
	                                                CALLBACKER_DEFAULT_PRIORITY,
	                                                _UT_callbacker_run_resolvable__helper__my_short,
	                                                &arg2));
	assert_true(callbacker_resolve_try_my_short(&rv2, &rrv2));

	assert_ptr_equal(callbacker_resolve_my_struct(&rv1), &arg1 + 2);
	assert_ptr_equal(callbacker_resolve_my_short(&rv2), &arg2 + 1);


	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

static void *
_UT_callbacker_resolve__helper(void * CALLBACKER_NULLABLE ip)
{
	return ip;
}

static void
UT_callbacker_resolve()
{
	enum { buf_size = 2000, resolve_count = 10 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval ress[resolve_count];

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(2));

	for (long i = 0 ; i < resolve_count ; i++)
		assert_false(callbacker_run_resolvable(&ress[i],
		                                       CALLBACKER_DEFAULT_PRIORITY,
		                                       _UT_callbacker_resolve__helper,
		                                       (void*)i));

	for (long i = 0 ; i < resolve_count/2 ; i++)
		assert_int_equal(i, (long)callbacker_resolve(&ress[i]));

	for (long i = 0 ; i < resolve_count/2 ; i++)
		assert_false(callbacker_run_resolvable(&ress[i],
		                                       CALLBACKER_DEFAULT_PRIORITY,
		                                       _UT_callbacker_resolve__helper,
		                                       (void*)i));

	for (long i = 0 ; i < resolve_count ; i++)
		assert_int_equal(i, (long)callbacker_resolve(&ress[i]));

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

static void *
_UT_callbacker_resolve_try__double_free__helper(void * CALLBACKER_NULLABLE ip CALLBACKER_UNUSED)
{
	return (void*)0xf00dcafe;
}

static void
UT_callbacker_resolve_try__double_free()
{
	enum { buf_size = 200 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval res;
	void * rv = NULL;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(2));

	assert_false(callbacker_run_resolvable(&res,
	                                       CALLBACKER_LOW_PRIORITY,
	                                       _UT_callbacker_resolve_try__double_free__helper,
	                                       NULL));

	reasonable_sleep();
	assert_false((long)callbacker_resolve_try(&res, &rv));
	assert_ptr_equal(rv, (void*)0xf00dcafe);
	expect_assert_failure((rv = (void*)callbacker_resolve_try(&res, &rv)));

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

static void *
_UT_callbacker_resolve__double_free__helper(void * CALLBACKER_NULLABLE ip CALLBACKER_UNUSED)
{
	return (void*)0xf00d;
}

static void
UT_callbacker_resolve__double_free()
{
	enum { buf_size = 200 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval res;
	void * tmp CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(2));

	assert_false(callbacker_run_resolvable(&res,
	                                       CALLBACKER_LOW_PRIORITY,
	                                       _UT_callbacker_resolve__double_free__helper,
	                                       NULL));

	assert_int_equal(0xf00d, (long)callbacker_resolve(&res));
	expect_assert_failure((tmp = callbacker_resolve(&res)));

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

static void
UT_callbacker_resolve__uninitialized()
{
	enum { buf_size = 200 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval res;
	void * tmp CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_false(callbacker_run_resolvable(&res,
	                                       CALLBACKER_LOW_PRIORITY,
	                                       resolvable_dummy_sleeper,
	                                       (void*)0x1));
	reasonable_sleep();
	callbacker_uninitialize();

	expect_assert_failure((tmp = callbacker_resolve(&res)));

	test_free(unit_buffer);
}

/***********************************************************************/

static void
UT_callbacker_resolve__NULL()
{
	enum { buf_size = 200 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval res;
	void * tmp CALLBACKER_UNUSED;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_false(callbacker_run_resolvable(&res,
	                                       CALLBACKER_LOW_PRIORITY,
	                                       resolvable_dummy_sleeper,
	                                       (void*)0x1));

	expect_assert_failure((tmp = callbacker_resolve(SNEAKY_NULL)));
	assert_ptr_equal(callbacker_resolve(&res), (void*)0x2);

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

struct _UT_callbacker_resolve_try_X__struct
{
	int a, b, c;
	char d;
};

static struct _UT_callbacker_resolve_try_X__struct *
_UT_callbacker_resolve_try_X__helper__yet_another_struct(
		struct _UT_callbacker_resolve_try_X__struct * CALLBACKER_NULLABLE sp)
{
	assert(sp);

	return sp + 2;
}

static short *
_UT_callbacker_resolve_try_X__helper__yet_another_short(short * CALLBACKER_NULLABLE sp CALLBACKER_UNUSED)
{
	assert(sp);

	return sp + 1;
}

CALLBACKER_RETVAL(struct _UT_callbacker_resolve_try_X__struct, yet_another_struct)
CALLBACKER_RETVAL(short, yet_another_short)

static void
UT_callbacker_resolve_try_X()
{
	enum { buf_size = 250 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval_yet_another_struct rv1;
	struct callbacker_retval_yet_another_short rv2;
	struct _UT_callbacker_resolve_try_X__struct arg1, *arg1_res = NULL;
	short arg2, *arg2_res = NULL;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_false(callbacker_run_resolvable_yet_another_struct(&rv1,
				CALLBACKER_DEFAULT_PRIORITY,
				_UT_callbacker_resolve_try_X__helper__yet_another_struct,
				&arg1));

	assert_true(callbacker_resolve_try_yet_another_struct(&rv1, &arg1_res));
	assert_ptr_equal(arg1_res, NULL);

	assert_false(callbacker_run_resolvable_yet_another_short(&rv2,
				CALLBACKER_DEFAULT_PRIORITY,
				_UT_callbacker_resolve_try_X__helper__yet_another_short,
				&arg2));

	assert_true(callbacker_resolve_try_yet_another_short(&rv2, &arg2_res));
	assert_ptr_equal(arg2_res, NULL);

	assert_false(callbacker_run(CALLBACKER_LOW_PRIORITY, _stop, NULL));
	callbacker_exec();

	assert_false(callbacker_resolve_try_yet_another_struct(&rv1, &arg1_res));
	assert_false(callbacker_resolve_try_yet_another_short(&rv2, &arg2_res));

	assert_ptr_equal(arg1_res, &arg1 + 2);
	assert_ptr_equal(arg2_res, &arg2 + 1);

	test_free(unit_buffer);
}

/***********************************************************************/

struct _UT_callbacker_resolve_X__struct
{
	int a, b, c;
	char d;
};

static struct _UT_callbacker_resolve_X__struct *
_UT_callbacker_resolve_X__helper__another_struct(
		struct _UT_callbacker_resolve_X__struct * CALLBACKER_NULLABLE sp)
{
	assert(sp);

	return sp + 2;
}

static short *
_UT_callbacker_resolve_X__helper__another_short(short * CALLBACKER_NULLABLE sp CALLBACKER_UNUSED)
{
	assert(sp);

	return sp + 1;
}

CALLBACKER_RETVAL(struct _UT_callbacker_resolve_X__struct, another_struct)
CALLBACKER_RETVAL(short, another_short)

static void
UT_callbacker_resolve_X()
{
	enum { buf_size = 400 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval_another_struct rv1;
	struct callbacker_retval_another_short rv2;
	struct callbacker_retval_another_struct rv3;
	struct callbacker_retval_another_short rv4;
	struct _UT_callbacker_resolve_X__struct arg1;
	short arg2;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_false(callbacker_run_resolvable_another_struct(&rv1,
	                                                      CALLBACKER_DEFAULT_PRIORITY,
	                                                      _UT_callbacker_resolve_X__helper__another_struct,
	                                                      &arg1));

	assert_false(callbacker_run_resolvable_another_short(&rv2,
	                                                     CALLBACKER_DEFAULT_PRIORITY,
	                                                     _UT_callbacker_resolve_X__helper__another_short,
	                                                     &arg2));

	assert_false(callbacker_run_resolvable_another_struct(&rv3,
	                                                      CALLBACKER_DEFAULT_PRIORITY,
	                                                      _UT_callbacker_resolve_X__helper__another_struct,
	                                                      &arg1));

	assert_false(callbacker_run_resolvable_another_short(&rv4,
	                                                     CALLBACKER_DEFAULT_PRIORITY,
	                                                     _UT_callbacker_resolve_X__helper__another_short,
	                                                     &arg2));

	assert_ptr_equal(callbacker_resolve_another_struct(&rv1), &arg1 + 2);
	assert_ptr_equal(callbacker_resolve_another_short(&rv2), &arg2 + 1);
	assert_ptr_equal(callbacker_resolve_another_struct(&rv3), &arg1 + 2);
	assert_ptr_equal(callbacker_resolve_another_short(&rv4), &arg2 + 1);

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

static void
UT_callbacker_resolve_try__NULL()
{
	enum { buf_size = 200 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval res;
	void * rv = NULL;
	bool CALLBACKER_UNUSED dummy;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_false(callbacker_run_resolvable(&res,
	                                       CALLBACKER_LOW_PRIORITY,
	                                       resolvable_dummy_sleeper,
	                                       NULL));

	expect_assert_failure((dummy = callbacker_resolve_try(&res, SNEAKY_NULL)));
	expect_assert_failure((dummy = callbacker_resolve_try(SNEAKY_NULL, &rv)));
	assert_int_equal(0, (long)callbacker_resolve(&res));

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

static void
UT_callbacker_resolve_try__uninitialized()
{
	enum { buf_size = 200 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval res;
	void * rv = NULL;
	bool CALLBACKER_UNUSED dummy;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(1));

	assert_false(callbacker_run_resolvable(&res,
	                                       CALLBACKER_LOW_PRIORITY,
	                                       resolvable_dummy_sleeper,
	                                       NULL));
	reasonable_sleep();
	callbacker_uninitialize();

	expect_assert_failure((dummy = callbacker_resolve_try(&res, &rv)));

	test_free(unit_buffer);
}

/***********************************************************************/

static void *
_UT_callbacker_resolve_try__helper(void * CALLBACKER_NULLABLE ip)
{
	if ((long)ip == 2)
	{
		reasonable_sleep();
		reasonable_sleep();
	}

	return ip;
}

static void
UT_callbacker_resolve_try()
{
	enum { buf_size = 2000, resolve_count = 5, loop_count = 3};
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);
	struct callbacker_retval ress[resolve_count];
	void * tmp;

	assert_false(callbacker_initialize(unit_buffer, buf_size));
	assert_false(callbacker_threads_start(3));

	for (int loop = 0 ; loop < loop_count ; loop++)
	{
		for (long i = 0 ; i < resolve_count ; i++)
			assert_false(callbacker_run_resolvable(&ress[i],
						CALLBACKER_DEFAULT_PRIORITY,
						_UT_callbacker_resolve_try__helper,
						(void*)i));

		reasonable_sleep();

		for (long i = 0 ; i < resolve_count ; i++)
		{
			if (i == 2)
			{
				assert_true(callbacker_resolve_try(&ress[i], &tmp));
				assert_true(callbacker_resolve_try(&ress[i], &tmp));
				assert_true(callbacker_resolve_try(&ress[i], &tmp));
				assert_true(callbacker_resolve_try(&ress[i], &tmp));
			}
			else
			{
				assert_false(callbacker_resolve_try(&ress[i], &tmp));
				assert_int_equal(i, (long)tmp);
			}
		}

		reasonable_sleep();
		reasonable_sleep();

		assert_false(callbacker_resolve_try(&ress[2], &tmp));
		assert_int_equal(2, (long)tmp);
	}

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

struct
{
	char buffer['z' - 'a' + 1];
	char * cursor;
} _FT_scheduling_order__meta;

void _FT_scheduling_order__meta__init()
{
	memset(_FT_scheduling_order__meta.buffer, 0, sizeof(_FT_scheduling_order__meta.buffer));
	_FT_scheduling_order__meta.cursor = _FT_scheduling_order__meta.buffer;
}

static void
_FT_scheduling_order__helper(void * CALLBACKER_NULLABLE i)
{
	*_FT_scheduling_order__meta.cursor = (char)(long)i;
	_FT_scheduling_order__meta.cursor++;
}

static void
FT_scheduling_order()
{
	enum { buf_size = 2000 };
	char * unit_buffer = test_malloc(buf_size);
	assert_non_null(unit_buffer);

	_FT_scheduling_order__meta__init();
	assert_false(callbacker_initialize(unit_buffer, buf_size));

	for (long i = 'a'; i <= 'z'; i++)
		assert_false(callbacker_run(CALLBACKER_DEFAULT_PRIORITY, _FT_scheduling_order__helper, (void*)i));

	callbacker_exec_try();

#if CALLBACKER_FAIR_SCHEDULING == 1
	printf("Asserting fair scheduling order.\n");
	assert_string_equal(_FT_scheduling_order__meta.buffer, "abcdefghijklmnopqrstuvwxyz");
#else
	printf("Asserting \"unfair\" scheduling order.\n");
	assert_string_equal(_FT_scheduling_order__meta.buffer, "zyxwvutsrqponmlkjihgfedcba");
#endif

	callbacker_uninitialize();
	test_free(unit_buffer);
}

/***********************************************************************/

static void
FT_TYPE_enums_behaviour()
{
	assert(CALLBACKER_IO_TYPE_READWRITE == (CALLBACKER_IO_TYPE_READ | CALLBACKER_IO_TYPE_WRITE));

	assert(CALLBACKER_IO_TYPE_ERROR & CALLBACKER_IO_TYPE_HANGUP);

	assert(CALLBACKER_IO_TYPE_ANY & CALLBACKER_IO_TYPE_HANGUP);
	assert(CALLBACKER_IO_TYPE_ANY & CALLBACKER_IO_TYPE_READ);
	assert(CALLBACKER_IO_TYPE_ANY & CALLBACKER_IO_TYPE_WRITE);
}

/***********************************************************************/

int
main(int ac, char ** av)
{
	const struct CMUnitTest tests[] =
	{
		cmocka_unit_test(UT_callbacker_initialize),
		cmocka_unit_test(UT_callbacker_initialize__NULL),
		cmocka_unit_test(UT_callbacker_initialize__twice),

		cmocka_unit_test(UT_callbacker_uninitialize),
		cmocka_unit_test(UT_callbacker_uninitialize__uninitialized),
		cmocka_unit_test(UT_callbacker_uninitialize__running),

		cmocka_unit_test(UT_callbacker_threads_start),
		cmocka_unit_test(UT_callbacker_threads_start__scheduling),
		cmocka_unit_test(UT_callbacker_threads_start__NULL),
		cmocka_unit_test(UT_callbacker_threads_start__uninitialized),

		cmocka_unit_test(UT_callbacker_threads_stop),
		cmocka_unit_test(UT_callbacker_threads_stop__nonexisting),
		cmocka_unit_test(UT_callbacker_threads_stop__nonrunning),
		cmocka_unit_test(UT_callbacker_threads_stop__uninitialized),

		cmocka_unit_test(UT_callbacker_threads_count_set),
		cmocka_unit_test(UT_callbacker_threads_count_set__uninitialized),

		cmocka_unit_test(UT_callbacker_threads_count_get),
		cmocka_unit_test(UT_callbacker_threads_count_get__uninitialized),

		cmocka_unit_test(UT_callbacker_run),
		cmocka_unit_test(UT_callbacker_run__priorities),
		cmocka_unit_test(UT_callbacker_run__NULL),
		cmocka_unit_test(UT_callbacker_run__memory),
		cmocka_unit_test(UT_callbacker_run__uninitialized),
		cmocka_unit_test(UT_callbacker_run__invalid_args),
		cmocka_unit_test(UT_callbacker_run__parent),

		cmocka_unit_test(UT_callbacker_io_start__read),
		cmocka_unit_test(UT_callbacker_io_start__write),
		// TBI: cmocka_unit_test(UT_callbacker_io_start__RW), // both at the same time
		cmocka_unit_test(UT_callbacker_io_start__chained),
		cmocka_unit_test(UT_callbacker_io_start__no_threads),
		cmocka_unit_test(UT_callbacker_io_start__NULL),
		cmocka_unit_test(UT_callbacker_io_start__invalid_args),
		cmocka_unit_test(UT_callbacker_io_start__HANGUP),
		cmocka_unit_test(UT_callbacker_io_start__cleanup),
		cmocka_unit_test(UT_callbacker_io_start__reuse_fds),
		cmocka_unit_test(UT_callbacker_io_start__close),
		cmocka_unit_test(UT_callbacker_io_start__memory),
		cmocka_unit_test(UT_callbacker_io_start__priorities),
		cmocka_unit_test(UT_callbacker_io_start__uninitialized),
		cmocka_unit_test(UT_callbacker_io_start__alternate_by_retval),
		// TODO: test for starvation
		cmocka_unit_test(UT_callbacker_io_start__start_twice),

		cmocka_unit_test(UT_callbacker_io_trigger),
		cmocka_unit_test(UT_callbacker_io_trigger__NULL),
		cmocka_unit_test(UT_callbacker_io_trigger__uninitialized),
		cmocka_unit_test(UT_callbacker_io_trigger__trigger_running),

		cmocka_unit_test(UT_callbacker_timer_trigger),
		cmocka_unit_test(UT_callbacker_timer_trigger__uninitialized),
		cmocka_unit_test(UT_callbacker_timer_trigger__NULL),
		cmocka_unit_test(UT_callbacker_timer_trigger__invalid),
		cmocka_unit_test(UT_callbacker_timer_trigger__stop_running),

		cmocka_unit_test(UT_callbacker_signal_start),
		cmocka_unit_test(UT_callbacker_signal_start__uninitialized),
		cmocka_unit_test(UT_callbacker_signal_start__NULL),
		cmocka_unit_test(UT_callbacker_signal_start__memory),
		cmocka_unit_test(UT_callbacker_signal_start__already_running),

		cmocka_unit_test(UT_callbacker_exec),
		cmocka_unit_test(UT_callbacker_exec__uninitialized),

		cmocka_unit_test(UT_callbacker_exec_try),
		cmocka_unit_test(UT_callbacker_exec_try__uninitialized),
		cmocka_unit_test(UT_callbacker_exec_try__multiple),

		cmocka_unit_test(UT_callbacker_timer_start),
		cmocka_unit_test(UT_callbacker_timer_start__NULL),
		cmocka_unit_test(UT_callbacker_timer_start__uninitialized),
		cmocka_unit_test(UT_callbacker_timer_start__memory),
		cmocka_unit_test(UT_callbacker_timer_start__invalid),
		cmocka_unit_test(UT_callbacker_timer_start__return_true),

		cmocka_unit_test(UT_callbacker_data_size),

		cmocka_unit_test(UT_callbacker_run_resolvable),
		cmocka_unit_test(UT_callbacker_run_resolvable__NULL),
		cmocka_unit_test(UT_callbacker_run_resolvable__unintialized),
		cmocka_unit_test(UT_callbacker_run_resolvable__memory),
		cmocka_unit_test(UT_callbacker_run_resolvable_X),

		cmocka_unit_test(UT_callbacker_resolve),
		cmocka_unit_test(UT_callbacker_resolve__double_free),
		cmocka_unit_test(UT_callbacker_resolve__NULL),
		cmocka_unit_test(UT_callbacker_resolve__uninitialized),
		cmocka_unit_test(UT_callbacker_resolve_X),

		cmocka_unit_test(UT_callbacker_resolve_try),
		cmocka_unit_test(UT_callbacker_resolve_try__double_free),
		cmocka_unit_test(UT_callbacker_resolve_try__NULL),
		cmocka_unit_test(UT_callbacker_resolve_try__uninitialized),
		cmocka_unit_test(UT_callbacker_resolve_try_X),

		cmocka_unit_test(FT_scheduling_order),
		cmocka_unit_test(FT_TYPE_enums_behaviour)
	};

	char * v = getenv("VALGRIND");

	if (v && atoi(v) > 0)
	{
		VALGRIND = true;
		half_sec.tv_sec = 5;
		half_sec.tv_nsec = 0;
	}

	if (ac > 1)
		cmocka_set_test_filter(av[1]);

	return cmocka_run_group_tests(tests, NULL, NULL);
}


/***********************************************************************/
