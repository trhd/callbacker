/**
 * callbacker -- A multithreaded callback queue / event loop library.
 * Copyright (C) 2018-2019 Hemmo Nieminen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "callbacker.h"

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdatomic.h>
#include <stdio.h>
#include <stdlib.h>
#include <optargs.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/sysinfo.h>
#include <unistd.h>

#define ABOUT "An echo server application for testing purposes."

static atomic_int ERROR = 0;

enum options
{
	OPTION_MODE,
	OPTION_HELP,
	_OPTION_COUNT
};

enum operating_mode
{
	_MODE_START,
	MODE_NORMAL,
	MODE_GLITCH,
	_MODE_END
};

static struct optargs_option OPTIONS[] =
{
	[OPTION_MODE] =
	{
		.long_option = "mode",
		.description = "Choose the running mode of the echo server.",
		.type = optargs_mandatory_option,
		.argument = (struct optargs_argument [])
		{
			[_MODE_START] =
			{
				.name = "mode",
				.type = optargs_argument_group
			},

			[MODE_NORMAL] =
			{
				.name = "normal",
				.description = "The normal running mode (the default).",
				.type = optargs_argument_group_member
			},

			[MODE_GLITCH] =
			{
				.name = "glitch",
				.description = "Purposefully insert errors into responses.",
				.type = optargs_argument_group_member
			},

			[_MODE_END] = optargs_argument_eol
		}
	},

	[OPTION_HELP] =
	{
		.long_option = "help",
		.short_option = 'h',
		.description = "Print this help text.",
		.type = optargs_terminating_option
	},

	[_OPTION_COUNT] = optargs_option_eol
};

struct client
{
	unsigned buffer_len;
	unsigned buffer_offset;
	char buffer[1024];
	atomic_int * errors;
};

static struct client *
new_client(atomic_int * e)
{
	assert(e);

	struct client * c = malloc(sizeof(struct client));

	if (c)
	{
		c->buffer_len = 0;
		c->buffer_offset = 0;
		c->errors = e;
	}

	return c;
}

static bool
handle_client_write(int client_socket, struct client * clt)
{
	assert(client_socket > 0);
	assert(clt);

	static unsigned long glitch_count = 0;
	ssize_t y;

	if (OPTIONS[OPTION_MODE].argument->match == &OPTIONS[OPTION_MODE].argument[MODE_GLITCH])
		clt->buffer[clt->buffer_offset + (glitch_count++ % (clt->buffer_len - clt->buffer_offset))] ^= 0xff;

	y = write(client_socket, clt->buffer + clt->buffer_offset, clt->buffer_len - clt->buffer_offset);

	if (y < 0)
		return true;

	clt->buffer_offset += y;

	if (clt->buffer_len - clt->buffer_offset == 0)
		clt->buffer_len = clt->buffer_offset = 0;

	return false;
}

static bool
handle_client_read(int client_socket, struct client * clt)
{
	assert(client_socket > 0);
	assert(clt);

	ssize_t y = read(client_socket, clt->buffer + clt->buffer_offset, sizeof(clt->buffer) - clt->buffer_len);

	if (y >= 0)
		clt->buffer_len += y;
	else if (errno != EAGAIN)
		return true;
	return false;
}

static enum callbacker_io_type
handle_client(int client_socket, enum callbacker_io_type type, void * user_data)
{
	assert(client_socket > 0);
	assert(user_data);

	struct client * clt = (struct client *)user_data;

	if (!(type & CALLBACKER_IO_TYPE_ERROR))
	{
		if (type & CALLBACKER_IO_TYPE_WRITE
		         && handle_client_write(client_socket, clt))
		{ /* ERROR */ }
		else if (type & CALLBACKER_IO_TYPE_READ && sizeof(clt->buffer) > clt->buffer_len
		         && handle_client_read(client_socket, clt))
		{ /* ERROR */ }
		else
		{
			return ((sizeof(clt->buffer) > clt->buffer_len) ? CALLBACKER_IO_TYPE_READ : 0)
				   | ((clt->buffer_len > 0) ? CALLBACKER_IO_TYPE_WRITE : 0);
		}

		printf("Encoutered an error while handling client connection.\n");
		atomic_fetch_add(clt->errors, 1);
	}

	printf("Closing client connection.\n");
	close(client_socket);
	free(clt);
	return CALLBACKER_IO_TYPE_NONE;
}

static enum callbacker_io_type
handle_connection(int server_socket, enum callbacker_io_type type, void * user_data)
{
	assert(server_socket > 0);
	assert(type & CALLBACKER_IO_TYPE_READ);
	assert(user_data);

	int client_socket = 0;
	atomic_int * error = (atomic_int *)user_data;
	struct client * c = NULL;

	printf("Handling an incoming connection.\n");

	if (type & CALLBACKER_IO_TYPE_ERROR)
		printf("callbacker detected an error on the server socket.\n");
	else if (!(client_socket = accept(server_socket, NULL, 0)))
		perror("accept");
	else if (fcntl(client_socket, F_SETFL, O_NONBLOCK) < 0)
		perror("fcntl");
	else if (!(c = new_client(error)))
		printf("Failed to allocate memory for a new client.\n");
	else if (!callbacker_io_start(client_socket, CALLBACKER_IO_TYPE_READ,
				CALLBACKER_DEFAULT_PRIORITY, handle_client, c))
		perror("callbacker_io_start");
	else
		return CALLBACKER_IO_TYPE_KEEP;

	atomic_fetch_add(error, 1);
	close(client_socket);
	free(c);
	callbacker_threads_stop(0);
	return CALLBACKER_IO_TYPE_NONE;
}

static bool
print_socket(int socket)
{
	struct sockaddr_storage ss = {0}; // Initialize this for scan-build.
	socklen_t sl = sizeof(ss);

	if (getsockname(socket, (struct sockaddr *)&ss, &sl) < 0)
		return true;

	assert(ss.ss_family == AF_INET);
	printf("port: %d\n", ntohs(((struct sockaddr_in *)&ss)->sin_port));
	fflush(stdout);

	return false;
}

static void
handle_signal(int signal, void * arg __attribute__((unused)))
{
	printf("User sent signal %d: %s.\n", signal, strsignal(signal));
	fflush(NULL);

	callbacker_threads_stop(0);
}

static bool
prepare_socket(int * sock)
{
	assert(sock);

	*sock = socket(AF_INET, SOCK_STREAM, 0);

	if (!*sock || listen(*sock, 50))
		return true;

	print_socket(*sock);
	return false;
}

int
main(int argc, char ** argv)
{
	char buffer[10000];
	int server_socket;
	int const nproc = get_nprocs();
	int idx = optargs_parse_options(argc, (char const **)argv, OPTIONS);

	if (idx < 0 || optargs_option_count(&OPTIONS[OPTION_HELP]))
	{
		optargs_print_help(argv[0], ABOUT, OPTIONS, NULL);
		return idx < 0 ? EXIT_FAILURE : EXIT_SUCCESS;
	}

	if (prepare_socket(&server_socket)
			|| callbacker_initialize(buffer, sizeof(buffer))
			|| (callbacker_signal_start(CALLBACKER_HIGH_PRIORITY,
					handle_signal, NULL, SIGTERM, SIGQUIT, SIGINT),
				!callbacker_io_start(server_socket, CALLBACKER_IO_TYPE_READ,
					CALLBACKER_DEFAULT_PRIORITY, handle_connection, &ERROR))
			|| (nproc > 1 && callbacker_threads_start(nproc - 1)))
	{
		printf("Failed to initialize the echo server.\n");
		return EXIT_FAILURE;
	}

	callbacker_exec();
	callbacker_uninitialize();

	return ERROR ? EXIT_FAILURE : EXIT_SUCCESS;
}
