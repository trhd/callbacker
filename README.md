# callbacker

## What is callbacker?

Callbacker is a scheduling/event loop/thread pool library written in C.
Callbacker works by appending work items (callbacks) into prioritized work
queues and by then processing those from a (possibly) dedicated thread(s).

Work items/callbacks can also be conditioned to run either periodically,
when there is IO activity in a file descriptor or when a signal is caught.

## Why not use ${X} instead of creating this one?

What's the fun in that?

This whole project started sort of by accident while working on another
project and at some point I just wanted to "complete" this.

## Status

Compiles! Ship it! / *Works for me*.

### Todo

* Limit how many X's can be running concurrently.
  - Introduce constraints and `callbacker_run_constrained()` (or similar)
    that can be e.g. used to control the number of concurrently running
    tasks working on a scarce resource. The constraint could then even be
    shared between different tasks. Constraints would always allow queueing
    new task but will throttle their concurrent execution.
* Consider allowing many instances of the callbacker.
* Define the maximum amount of memory callbacker is allowed to use and
  allocate it by mapping virtual memory (the amount can then be large and
  the memory will not be used until the virtual memory is actually taken
  into use).
* Run same tasks on same threads/processors for cache locality benefits.
* *Consider* changing thread count to be the maxiumum number of threads
  and shutting down idle threads.
  - Thread waking up to find no work X times in a row -> idle thread and
	can bu shut down
  - Thread waiting/polling for more work and finding some immediately X
	times in a row -> thread busy and could use another one.
* Manage all timers with a single timerfd to save resources.
* Consider adding `callbacker_resolve_any()` function.

## How can I use this?

Install this as a shared library or use it as a Meson submodule (or don't
but then you are on your own).

### Examples

- [Unit tests](tst/unit_tests.c).
- An [echo server](tst/echo_server.c).

## Where can I use this?

Currently epoll is a requirement (i.e. works only on Linux).

## Found a fault / Have constructive feedback

Please let me know. Best ways to contact me probably via Gitlab or [email](mailto:hemmo.nieminen@iki.fi).
