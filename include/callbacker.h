/**
 * callbacker -- A multithreaded callback queue / event loop library.
 * Copyright (C) 2018-2019 Hemmo Nieminen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "callbacker_config.h"

#include <dsac/atomic_debug_flags.h>
#include <dsac/condition.h>
#include <dsac/lock.h>
#include <signal.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>

#ifndef CALLBACKER_QUEUE_COUNT
# error The number of callbacker work queues / priority levels has not been defined.
#endif
/**
 * Some priority level definitions.
 *
 * These are deduced from the CALLBACKER_QUEUE_COUNT from above.
 */
#define CALLBACKER_LOW_PRIORITY ( (CALLBACKER_QUEUE_COUNT / 2))
#define CALLBACKER_DEFAULT_PRIORITY 0
#define CALLBACKER_HIGH_PRIORITY (-(CALLBACKER_QUEUE_COUNT / 2))

static_assert(CALLBACKER_QUEUE_COUNT % 2,
              "Number of callbacker queues / priority levels must be odd.");

/**
 * Typdefine a void pointer used specificly to identify a timer
 * "subscription".
 */
typedef void * callbacker_timer_id_t;

/**
 * Typdefine a void pointer used specificly to identify IO "subscription".
 */
typedef void * callbacker_io_id_t;

/**
 * This macro will expand to wrapper functions that can be used to use
 * callbacker_run_resolvable(), callbacker_resolve() and
 * callbacker_resolve_try() with other types of pointers than void
 * pointers.
 *
 * For each expansion, the following structure and functions will be
 * defined:
 *  - struct callbacker_retval_SUFFIX;
 *  - callbacker_run_resolvable_SUFFIX();
 *  - callbacker_resolve_SUFFIX();
 *  - callbacker_resolve_try_SUFFIX();
 *
 * The required arguments to the defined functions will be as in the
 * normal void* versions of the corresponding functions.
 */
#define CALLBACKER_RETVAL(TYPE, SUFFIX) \
	struct callbacker_retval_##SUFFIX \
	{ \
		struct lock lock; \
		struct condition cond; \
		atomic_bool done; \
		TYPE * CALLBACKER_NULLABLE (*CALLBACKER_NONNULL fn)(TYPE * CALLBACKER_NULLABLE); \
		union \
		{ \
			TYPE * CALLBACKER_NULLABLE rv; \
			TYPE * CALLBACKER_NULLABLE arg; \
		} ptr; \
		ATOMIC_DEBUG_FLAGS(_CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_COUNT) \
	}; \
\
	static inline CALLBACKER_WARN_UNUSED_RESULT CALLBACKER_UNUSED \
	bool \
	callbacker_run_resolvable_##SUFFIX( \
	                struct callbacker_retval_##SUFFIX * CALLBACKER_NONNULL return_value, \
	                int8_t priority, \
	                TYPE * CALLBACKER_NULLABLE (*CALLBACKER_NONNULL callback)( \
	                                TYPE * CALLBACKER_NULLABLE user_data), \
	                TYPE * CALLBACKER_NULLABLE user_data) \
	{ \
		return callbacker_run_resolvable((struct callbacker_retval *)return_value, \
		                                 priority, \
		                                 (void * CALLBACKER_NULLABLE (*CALLBACKER_NONNULL)( \
		                                                 void * CALLBACKER_NULLABLE))callback, \
		                                 user_data); \
	} \
\
	static inline CALLBACKER_WARN_UNUSED_RESULT CALLBACKER_UNUSED \
	TYPE * \
	callbacker_resolve_##SUFFIX( \
	                struct callbacker_retval_##SUFFIX * CALLBACKER_NONNULL return_value) \
	{ \
		return (TYPE *)callbacker_resolve((struct callbacker_retval *)return_value); \
	} \
\
	static inline CALLBACKER_WARN_UNUSED_RESULT CALLBACKER_UNUSED \
	bool \
	callbacker_resolve_try_##SUFFIX( \
	                struct callbacker_retval_##SUFFIX * CALLBACKER_NONNULL return_value, \
	                TYPE ** CALLBACKER_NONNULL actual_return_value) \
	{ \
		return callbacker_resolve_try((struct callbacker_retval *)return_value, \
		                              (void *)actual_return_value); \
	}

/**
 * This is a debugging enum that is used internally within the
 * callbacker library.
 */
ATOMIC_DEBUG_FLAGS_ENUM(callbacker_retval_atomic_debug_flag,
                        CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_PENDING,
                        _CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_COUNT)

/**
 * This struct is used to store internal information when using
 * callbacker_run_resolvable() and callbacker_resolve() functions.
 */
struct callbacker_retval
{
	struct lock lock;
	struct condition cond;
	atomic_bool done;
	void * CALLBACKER_NULLABLE (*CALLBACKER_NONNULL fn)(void * CALLBACKER_NULLABLE);
	union
	{
		void * CALLBACKER_NULLABLE rv;
		void * CALLBACKER_NULLABLE arg;
	} ptr;
	ATOMIC_DEBUG_FLAGS(_CALLBACKER_RETVAL_ATOMIC_DEBUG_FLAG_COUNT)
};

/**
 * Supported IO types.
 *
 * These are used to define the types of IO activities that a IO callback
 * should be triggered on.
 */
enum callbacker_io_type
{
	/* No IO event. */
	CALLBACKER_IO_TYPE_NONE = 0,

	/* Indicates that data can be read from the associated file descriptor. */
	CALLBACKER_IO_TYPE_READ = (1 << 0),

	/* Indicates that data can be written to the associated file descriptor. */
	CALLBACKER_IO_TYPE_WRITE = (1 << 1),

	/* Matches both CALLBACKER_IO_TYPE_READ and CALLBACKER_IO_TYPE_WRITE. */
	CALLBACKER_IO_TYPE_READWRITE = CALLBACKER_IO_TYPE_READ | CALLBACKER_IO_TYPE_WRITE,

	/* Indicates that an error was detected on the associated file descriptor. */
	CALLBACKER_IO_TYPE_ERROR = (1 << 2),

	/* Indicates that a hangup was detected on the associated file descriptor. */
	CALLBACKER_IO_TYPE_HANGUP = ((1 << 3) | CALLBACKER_IO_TYPE_ERROR),

	/* Matches all the above non-zero conditions. */
	CALLBACKER_IO_TYPE_ANY = ((1 << 4) - 1),

	/* Can be used to indicate that no changes should be made to the
	 * triggering conditions. */
	CALLBACKER_IO_TYPE_KEEP = (1 << 5),

	/* Indicates that a callback was explicitly triggered by the user. */
	CALLBACKER_IO_TYPE_TRIGGER = (1 << 6)
};

/**
 * The types of events that can trigger a timer.
 */
enum callbacker_timer_type
{
	/* No timer event. */
	CALLBACKER_TIMER_TYPE_NONE = 0,

	/* Indicates that the callback was invoked due to a timer expiring. */
	CALLBACKER_TIMER_TYPE_EXPIRATION,

	/* Indicates that a callback was explicitly triggered by the user. */
	CALLBACKER_TIMER_TYPE_TRIGGER
};

/**
 * Initialize callbacker.
 *
 * Arguments:
 *   cache_buffer:        The memory area from where to allocate memory
 *                        for callbacker's internal needs. This must be
 *                        available as long as callbacker is being used.
 *   cache_buffer_length: The size of the above cache_buffer memory area.
 *
 * Return value:
 *   True in case of an error. False otherwise.
 */
CALLBACKER_WARN_UNUSED_RESULT
bool
callbacker_initialize(void * CALLBACKER_NONNULL cache_buffer, size_t cache_buffer_length);

/**
 * Uninitialize callbacker and free all used resources.
 *
 * Return value:
 *   True in case of an error. False otherwise.
 */
void
callbacker_uninitialize(void);

/**
 * Get the size of an individual callbacker's internal data holder/unit.
 *
 * Return value:
 *   The requested size in bytes.
 */
CALLBACKER_WARN_UNUSED_RESULT
size_t
callbacker_data_size(void);

/**
 * Start dedicated worker threads in the background.
 *
 * Arguments:
 *   count: The number of threads to create. Must be greater than zero.
 *
 * Return value:
 *   True in case of an error. False otherwise.
 */
CALLBACKER_WARN_UNUSED_RESULT
bool
callbacker_threads_start(unsigned int count);

/**
 * Stop running worker threads.
 *
 * Arguments:
 *   count: The number of threads to stop. Providing zero here will cause
 *          all running worker threads (including possibly exec'd ones) to
 *          be stopped.
 *
 * Return valuse:
 *   True in case of an error. False otherwise.
 */
void
callbacker_threads_stop(unsigned int count);

/**
 * Set the number of running worker threads.
 *
 * Argument:
 *   count: The number of woker threads to be left running (a possible
 *          exec'd thread is also counted).
 *
 * Return valuse:
 *   True in case of an error. False otherwise.
 */
CALLBACKER_WARN_UNUSED_RESULT
bool
callbacker_threads_count_set(unsigned int thread_count);

/**
 * The the number of running worker threads.
 *
 * A possible exec'd thread will be included in the count.
 *
 * Return value:
 *   The number of running worker threads.
 */
CALLBACKER_WARN_UNUSED_RESULT
unsigned int
callbacker_threads_count_get(void);

/**
 * Enter the callbacker's main loop to process work items from the
 * callbacker's queues and return only when callbacker is completely shut
 * down (the number of "worker" threads becomes zero).
 */
void
callbacker_exec(void);

/**
 * Enter the callbacker's main loop to process work items from the
 * callbacker's queues and return the instant there were no more tasks to
 * process. Note that this does not mean there would be nothing scheduled
 * to be run when this function returns, only that there were none when
 * the decicion to return was made.
 */
bool
callbacker_exec_try(void);

/**
 * Schedule(/offload) a callback.
 *
 * Run the given function with the given argument from a callbacker's
 * worker thread.
 *
 * Arguments:
 *   priority:  Priority/urgency of the scheduled callback. Must be an
 *              integer between CALLBACKER_LOW_PRIORITY and
 *              CALLBACKER_HIGH_PRIORITY. Scheduled callbacks with higher
 *              priority will be executed before other callbacks.
 *   callback:  The function that should be executed from a callbacker's
 *              work thread.
 *   user_data: An optional argument to pass additional information to the
 *              called callback function. This argument will be supplied
 *              to the callback function upon invocation.
 *
 * Return value:
 *   True in case of an error. False otherwise.
 */
CALLBACKER_WARN_UNUSED_RESULT
bool
callbacker_run(int8_t priority,
               void (*CALLBACKER_NONNULL callback)(void * CALLBACKER_NULLABLE user_data),
               void * CALLBACKER_NULLABLE user_data);

/**
 * Schedule(/offload) a callback which returns a value.
 *
 * Run the given function with the given argument from a callbacker's
 * worker thread. The returned value from the executed can be accessed via
 * the callbacker_resolve() function.
 *
 * Arguments:
 *   return_value: A pointer to a structure that will be used to store the
 *                 return value of the scheduled function. This structure
 *                 must be resolved with callbacker_resolve() (or some of
 *                 its variants) after being given here and must not be
 *                 modified before that has been done.
 *   priority:     Priority/urgency of the scheduled callback. Must be an
 *                 integer between CALLBACKER_LOW_PRIORITY and
 *                 CALLBACKER_HIGH_PRIORITY. Scheduled callbacks with
 *                 higher priority will be executed before other
 *                 callbacks.
 *   callback:     The function that should be executed from a callbacker's
 *                 work thread.
 *   user_data:    An optional argument to pass additional information to the
 *                 called callback function. This argument will be
 *                 supplied to the callback function upon invocation.
 *
 * Return value:
 *   True in case of an error. False otherwise.
 */
CALLBACKER_WARN_UNUSED_RESULT
bool
callbacker_run_resolvable(struct callbacker_retval * CALLBACKER_NONNULL return_value,
                          int8_t priority,
                          void * CALLBACKER_NULLABLE (*CALLBACKER_NONNULL callback)(void * CALLBACKER_NULLABLE user_data),
                          void * CALLBACKER_NULLABLE user_data);

/**
 * Resolve a return value from a function scheduled with
 * callbacker_run_resolvable().
 *
 * This function will block until the function scheduled with the given
 * return value struct has finished and its return value can be obtained.
 *
 * Arguments:
 *   return_value: A pointer to a return value structure that has been
 *                 used with a callbacker_run_resolvable() invocation
 *                 prior calling callbacker_resolve(). The structure is
 *                 free to be reused or freed after this function has
 *                 finished.
 *
 * Return value:
 *   The value the associated scheduled function returned will be
 *   returned.
 */
CALLBACKER_WARN_UNUSED_RESULT
void * CALLBACKER_NULLABLE
callbacker_resolve(struct callbacker_retval * CALLBACKER_NONNULL return_value);

/**
 * Try resolving a return value from a function scheduled with
 * callbacker_run_resolvable().
 *
 * This function will return the return value from the given return value
 * structure or fail if the requested value is not yet available.
 *
 * Arguments:
 *   return_value:        A pointer to a return value structure that has been
 *                        used with a callbacker_run_resolvable()
 *                        invocation prior to calling
 *                        callbacker_resolve(). The structure is free to
 *                        be reused or freed after this function has
 *                        finished if no errors occurred.
 *   actual_return_value: A pointer to a void pointer where the result
 *                        shuold be stored.
 *
 * Return value:
 *   False if no errors occurred and the requested return value was stored
 *   in the actual_return_value pointer.
 */
CALLBACKER_WARN_UNUSED_RESULT
bool
callbacker_resolve_try(struct callbacker_retval * CALLBACKER_NONNULL return_value,
                       void * CALLBACKER_NULLABLE * CALLBACKER_NONNULL actual_return_value);

/**
 * Schedule callbacks for IO activity on a file descriptor.
 *
 * Define a callback function to be invoked whenever data can be read
 * or written to a file descriptor.
 *
 * This function will block if called for a file descriptor that has
 * already been used to schedule callbacks. The execution will block
 * until the earlier callback scheduling is cancelled and the new one
 * can be started.
 *
 * Arguments:
 *   file_descriptor: The file descriptor that should be monitored for IO
 *                    activity.
 *   io_type:         The type of IO activity on the file descriptor that
 *                    should trigger the callback function.
 *   priority:        The priority with which the callbacks should be
 *                    scheduled/queued after the triggering conditions
 *                    have been met.
 *   callback:        The callback function that should be invoked. The
 *                    callback function will be supplied with the
 *                    following arguments upon invocation:
 *    file_descriptor:  The file descriptor that triggered the callback.
 *    io_type:          The type of IO activity that was detected on the
 *                      file descriptor.
 *    user_data:        The optional additional information given for the
 *                      callback during registration/scheduling of the
 *                      callback function.
 *    return type:      Should the invoked callback function return an IO
 *                      activity type other than CALLBACK_IO_TYPE_NONE,
 *                      the callback function will no longer trigger for
 *                      the returned IO activity type. When there is no
 *                      longer an IO activity type for which the callback
 *                      function would be triggered, the callback function
 *                      registration/scheduling will be completely removed
 *                      from callbacker and related resources freed.
 *   user_data:       An optional argument to pass additional information
 *                    to the callback function upon invocation.
 *
 *   Return value:
 *     An identifier (a pointer) that can be used to identify this
 *     particular callback "subscription". False/NULL in case of an error.
 */
CALLBACKER_WARN_UNUSED_RESULT
callbacker_io_id_t CALLBACKER_NULLABLE
callbacker_io_start(int file_descriptor,
                    enum callbacker_io_type io_type,
                    int8_t priority,
                    enum callbacker_io_type (*CALLBACKER_NONNULL callback)(
                                    int file_descriptor,
                                    enum callbacker_io_type io_type,
                                    void * CALLBACKER_NULLABLE user_data),
                    void * CALLBACKER_NULLABLE user_data);

/**
 * Trigger a callback for a previously scheduled IO.
 *
 * With this function it is possible to trigger a IO callback that has
 * earlier been scheduled with some conditions. This can be useful e.g.
 * when shutting down a daemon where resources cannot be freed
 * until all scheduled callbacks are guaranteed to not be called anymore.
 *
 * A callback that has been explicitly triggered with this function will
 * have CALLBACKER_IO_TYPE_TRIGGER in the event type argument when
 * invoked.
 *
 * Arguments:
 *   id: A pointer identifying the callback that should be triggered.
 */
void callbacker_io_trigger(callbacker_io_id_t CALLBACKER_NONNULL id);

/**
 * Schedule periodical callbacks.
 *
 * Define a callback function to be invoked after an initial delay and
 * optionally periodically after the initial invocation.
 *
 * Arguments:
 *   priority:        The priority with which the callbacks should be
 *                    scheduled/queued after the specified amount of time
 *                    has passed.
 *   delay:           The amount of time to wait before invoking the
 *                    callback for the first time.
 *   interval:        The amount of time to wait between two invocations
 *                    of the callback function.
 *   callback:        The callback function that should be invoked. The
 *                    user_data pointer (if given) will be given as an
 *                    argument to this function when invoked. Should the
 *                    invoked callback function return true, the timer is
 *                    cancelled and associated resources are freed.
 *   user_data:       An argument to pass additional information to the
 *                    callback function upon invocation (can be NULL).
 *
 * Return value:
 *   An identifier (a pointer) that can be used to identify this
 *   particular timer "subscription". False/NULL in case of an error.
 */
CALLBACKER_WARN_UNUSED_RESULT
callbacker_timer_id_t CALLBACKER_NULLABLE
callbacker_timer_start(int8_t priority,
                       struct timespec const * CALLBACKER_NONNULL delay,
                       struct timespec const * CALLBACKER_NULLABLE interval,
                       enum callbacker_timer_type (*CALLBACKER_NONNULL callback)(
                                       callbacker_timer_id_t CALLBACKER_NONNULL id,
                                       enum callbacker_timer_type type,
                                       void * CALLBACKER_NULLABLE user_data),
                       void * CALLBACKER_NULLABLE user_data);

/**
 * Trigger a callback for a previously scheduled timer.
 *
 * With this function it is possible to trigger a timer callback that has
 * earlier been scheduled with some conditions. This can be useful e.g.
 * when shutting down a daemon where resources cannot be freed
 * until all scheduled callbacks are guaranteed to not be called anymore.
 *
 * A callback that has been explicitly triggered with this function will
 * have CALLBACKER_TIMER_TYPE_TRIGGER in the event type argument when
 * invoked.
 *
 * Arguments:
 *   id: A pointer (callabacker_timer_id_t) identifying the callback that
 *       should be triggered.
 */
void
callbacker_timer_trigger(callbacker_timer_id_t CALLBACKER_NONNULL id);

/**
 * Schedule a callback function to be called when a signal is received.
 *
 * The callbacks for signal must be defined before starting any threads
 * for the callbacker and can not be removed withtout re-initializing the
 * callbacker.
 *
 * One should use the callbacker_signal_start convenience macro to use
 * this function.
 *
 * Arguments:
 *   priority:        The priority with which the callbacks should be
 *                    scheduled/queued after the specified signal has been
 *                    received.
 *   callback:        The callback function that should be invoked. The
 *                    callback function will be supplied with the
 *                    following arguments upon invocation:
 *    signal:           The signal that was received and triggered the
 *                      callback function's invocation
 *    user_data:        The optional additional information given for the
 *                      callback during registration/scheduling of the
 *                      signal handler.
 * ...:               List of singals for which the given callback
 *                    function should be called. The last signal/number in
 *                    the list must be a -1.
 */
void
_callbacker_signal_start(int8_t priority,
                         void (*CALLBACKER_NONNULL callback)(int signal, void * CALLBACKER_NULLABLE user_data),
                         void * CALLBACKER_NULLABLE user_data,
                         ...);

/**
 * A convenience macro for _callbacker_signal_start function.
 *
 * Otherwise the same as the function _callbacker_signal_start but the
 * ending signal number -1 is automatically added to the arguments.
 */
#define callbacker_signal_start(PRIORITY, CALLBACK, USER_DATA, ...) \
	_callbacker_signal_start(PRIORITY, CALLBACK, USER_DATA, __VA_ARGS__, -1)
