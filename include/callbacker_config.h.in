/**
 * callbacker -- A multithreaded callback queue / event loop library.
 * Copyright (C) 2018-2019 Hemmo Nieminen
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#mesondefine CALLBACKER_DEBUG

#mesondefine CALLBACKER_QUEUE_COUNT
#mesondefine CALLBACKER_FAIR_SCHEDULING
#mesondefine CALLBACKER_NULLABILITY_SUPPORT

#define CALLBACKER_WARN_UNUSED_RESULT __attribute__((warn_unused_result))
#define CALLBACKER_UNUSED __attribute__((unused))

#if CALLBACKER_NULLABILITY_SUPPORT == 1
#	pragma clang diagnostic ignored "-Wnullability-extension"
#	define CALLBACKER_NONNULL _Nonnull
#	define CALLBACKER_NULLABLE _Nullable
#else
#	define CALLBACKER_NONNULL
#	define CALLBACKER_NULLABLE
#endif

#undef CALLBACKER_NULLABILITY_SUPPORT
